﻿$(document).ready(function () {

    $("body").on("click", "#btnNew", function () {
        window.location = root + "Quartos/Novo";
    });
    
    $("input[type='text']").keydown(function (event) {
        if (event.which == 13) {
            $("#btnSearch").click();
        }
    });

    $("#btnView, #btnEdit").click(function () {
        if (!$(this).hasClass("disabled")) {
            window.location = root + "Quartos/EditarQuarto/" + $("#results input[type=checkbox]:checked").val();
        }

    });
    
    $("#btnDelete").click(function () {
        if (!$(this).hasClass("disabled")) {

            $.each($("#searchResult table tbody input[type=checkbox]:checked"), function (idx, item) {

                var url = root + "Quartos/ExcluirQuarto/" + $(item).val();

                $.get(url, null, function (data) {
                    ShowMessage(data, 5000);

                    if (data.Status == "Success") {
                        $(item).parent().parent().remove();
                    }
                });
            });
        }
    });

    $("#btnSearch").click(function () {
        var url = root + "Quartos/PesquisarQuartos";
        var searchTerms = GetSearchTerms();
        searchTerms.Page = 1;

        Paginate(url, searchTerms, "#results", GetSearchTerms);
    });
});

function GetSearchTerms() {
    var searchTerms = {
        SectorId: $("#SectorId option:selected").val(),
        Description: $("#Description").val()
    };

    return searchTerms;
}