﻿$(document).ready(function () {

    $("#Description").focus();

    if ($("#Id").val() > 0) {
        $("#bedList tbody tr :not(:hidden)").find("#btnRemoveBed").click(function() {
            $(this).parent().parent().remove();

            if ($("#bedList tbody tr:not(:first)").length == 0) {
                $("#bedList").fadeOut("slow");
            }
        });
        
        var roomType = $("#RoomType option:selected").val();

        if (roomType == 2) {
            $("#Beds").removeClass("hidden");
            $("#CollectiveRoom").removeClass("hidden");
        } else {
            $("#Beds").addClass("hidden");
            $("#CollectiveRoom").addClass("hidden");
        }
    }

   $("#btnCancel").click(function () {
        window.location = root + "Quartos/ListarQuartos";
    });

    $("#bedDesc").keydown(function(event) {
        if (event.keyCode == 13) {
            event.preventDefault();
            $("#btnAddBed").click();
        }
    });

    $("#RoomType").change(function () {
        var roomType = $(this).val();

        if (roomType == 2) {
            $("#Beds").removeClass("hidden");
            $("#CollectiveRoom").removeClass("hidden");
        } else {
            $("#Beds").addClass("hidden");
            $("#CollectiveRoom").addClass("hidden");
        }
    });

    $("#btnAddBed").click(function () {

        if (!$("#bedDesc").val()) {
            ShowMessage({ Status: "Error", Message: "O campo descrição do leito é obrigatório." }, 5000);
            return false;
        } else {
            var bed = $.grep($("#bedList tbody tr:not(:first)"), function (n) {
                var desc = $(n).find("td");

                if (desc.length > 0) {
                    return $("#bedDesc").val() == $(desc[0]).text();
                }

                return false;
            });

            if (bed.length > 0) {
                ShowMessage({ Status: "Error", Message: "Leito já adicionado." }, 5000);
                return false;
            }
        }

        var desc = $("#bedDesc").val();
        var newLine = $("#bedList tbody tr:first").clone();

        $(newLine.find("td")[0]).find("#BedDescription").val(desc)
            .maxlength({
                alwaysShow: true,
                threshold: 5,
                warningClass: "label label-success",
                limitReachedClass: "label label-important",
                separator: ' de ',
                preText: 'Faltam ',
                postText: ' caracteres faltando.',
                validate: true
            });

        newLine.find("#btnRemoveBed").click(function () {
            $(this).parent().parent().remove();

            if ($("#bedList tbody tr:not(:first)").length == 0) {
                $("#bedList").fadeOut("slow");
            }
        });

        newLine.removeClass("hidden");

        $("#bedList tbody").append(newLine);
        $("#bedList").fadeIn("slow");

        $("#bedDesc").val("").focus();
    });

    $("#btnSave").click(function () {

        var beds = [];

        $.each($("#bedList tbody tr:not(:first)"), function () {
            var desc = $($(this).find("td")[0]).find("#BedDescription").val();
            beds.push({ Id: 0, Value: desc });
        });

        var room = {
            Id: $("#Id").val(),
            Description: $("#Description").val(),
            SectorId: $("#SectorId").val(),
            MaxNumberOfPatients: 0,
            RoomType: $("input[name='RoomType']:checked").val(),
            Beds: beds
        };

        var url = $("#frmRoom").attr("action");

        $.ajaxSetup({
            dataType: "json",
            contentType: "application/json, charset=utf-8"
        });

        $.post(url, JSON.stringify(room), function (data) {
            ShowMessage(data, 5000);
        });

    });
    
    $("#btnBack").click(function () {
        window.location = root + "Quartos/ListarQuartos";
    });

    $("#btnDelete").click(function () {
        if (!$(this).hasClass("disabled")) {

            var url = root + "Quartos/ExcluirQuarto/" + $("#Id").val();

            $.get(url, null, function(data) {
                ShowMessage(data, 5000);
                
                if (data.Status == "Success") {
                    if ($("#Id").val() == 0) {
                        $("#Id").val(data.Id);
                        $("#title").text("Editar Quarto");
                        $("#breadCrumb").text("Editar Quarto");
                        $("#frmRoom").attr("action", data.FormAction);
                    }
                }
            });
        }
    });
});