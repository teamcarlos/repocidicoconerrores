﻿$(document).ready(function () {

    $("body").on("click", "#btnNew", function() {
        window.location = root + "Usuarios/Novo";
    });

    $("input").keydown(function(event) {
        if (event.which == 13) {
            $("#btnSearch").click();
        }
    });

    $("#btnView, #btnEdit").click(function () {
        if (!$(this).hasClass("disabled")) {
            window.location = root + "Usuarios/EditarUsuario/" + $("#results input[type=checkbox]:checked").val();
        }
        
    });

    $("#btnDelete").click(function() {
        if (!$(this).hasClass("disabled")) {

            $.each($("#searchResult table tbody input[type=checkbox]:checked"), function(idx, item) {

                var url = root + "Usuarios/ExcluirUsuario/" + $(item).val();

                $.get(url, null, function (data) {
                    ShowMessage(data, 5000);

                    if (data.Status == "Success") {
                        $(item).parent().parent().remove();
                    }
                });
            });
        }
    });

	$("#btnSearch").click(function () {
	    var url = root + "Usuarios/PesquisarUsuarios";
	    
		var searchTerms = GetSearchTerms();
		searchTerms.Page = 1;

		Paginate(url, searchTerms, "#results", GetSearchTerms);
	});

	function GetSearchTerms() {
		
		var searchTerms = {
			Name: $("#Name").val(),
			SectorId: $("#Sectors option:selected").val(),
			Page: 1
		};

		return searchTerms;
	}
	
});
