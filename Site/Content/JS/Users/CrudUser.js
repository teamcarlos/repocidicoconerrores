﻿/// <reference path="~/Scripts/jquery-2.0.3.intellisense.js" />
/// <reference path="~/Content/JS/Layout/Layout.js" />
/// <reference path="~/Content/JS/Util.js" />

$(document).ready(function () {

    $("#FirstName").focus();

    if ($("#Id").val() > 0) {
        $("#grouListItem table tbody tr:not(:hidden)").find("#btnRemoveGroup").click(function () {
            var row = $(this).parent().parent();
            $("#listGroups option[value=" + row.find("#GroupId").val() + "]").removeClass("hidden").prop("disabled", false);
            row.remove();
        });
    }

    $('#CPF').mask("999.999.999-99")
        .change(function () {
            var value = $(this).val();
            if (!ValidateCPF(value)) {
                ShowMessage({ Status: "Error", Message: "CPF inválido." }, 5000);
            } else {
            }
        });

    $("#BirthDate").mask("99/99/9999");

    $("#Email").change(function () {
        var value = $(this).val();
        if (!ValidateEmail(value)) {
            ShowMessage({ Status: "Error", Message: "E-mail inválido." }, 5000);
        } else {

        }
    });

    $("#btnAddGroup").on("click", function () {

        if ($("#listGroups option:selected").val() > 0) {

            var divSample = $("#grouListItem table tbody tr:hidden").clone();
            divSample.find("#GroupId").val($("#listGroups").val());
            divSample.find("#groupName").text($("#listGroups option:selected").text());

            $("#listGroups option:selected").addClass("hidden").prop("disabled", true);
            $("#listGroups option").eq(0).prop("selected", true);

            divSample.find("#btnRemoveGroup").click(function () {
                $("#listGroups option[value=" + divSample.find("#GroupId").val() + "]").removeClass("hidden").prop("disabled", false);
                divSample.remove();
            });

            $("#grouListItem table tbody").append(divSample);

            divSample.removeClass("hidden");
        }

    });

    $("#btnSave").click(function () {

        var url = $("#frmUser").attr("action");

        var groups = [];

        $.each($("#grouListItem table tbody tr:not(:hidden)"), function (idx, item) {
            var groupId = $(item).find("#GroupId").val();

            groups.push({ Id: groupId });
        });

        var user = {
            Id: $("#Id").val(),
            FirstName: $("#FirstName").val(),
            LastName: $("#LastName").val(),
            CPF: $("#CPF").val(),
            RG: $("#RG").val(),
            BirthDate: $("#BirthDate").val(),
            Email: $("#Email").val(),
            Record: $("#Record").val(),
            Login: $("#Login").val(),
            Password: $("#Password").val(),
            SectorId: $("#SectorId option:selected").val(),
            GroupsIds: groups
        };

        $.ajaxSetup({
            dataType: "json",
            contentType: "application/json, charset=utf-8"
        });

        $.post(url, JSON.stringify(user), function (data) {
            ShowMessage(data, 5000);

            if (data.Status == "Success") {
                if ($("#Id").val() == 0) {
                    $("#Id").val(data.Id);
                    $("#title").text("Editar Usuário");
                    $("#breadCrumb").text("Editar Usuário");
                    $("#frmUser").attr("action", data.FormAction);
                }
            }
        });

    });

    $("#btnBack").click(function () {
        window.location = root + "Usuarios/ListarUsuarios";
    });

});