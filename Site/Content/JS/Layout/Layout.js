﻿$(document).ready(function () {

    if (navigator.userAgent.match(/IEMobile\/10\.0/)) {
        var msViewportStyle = document.createElement("style");
        msViewportStyle.appendChild(document.createTextNode("@-ms-viewport{width:auto!important}"));
        document.getElementsByTagName("head")[0].appendChild(msViewportStyle);
    }

    $(".panel-heading a").click(function() {
        var icon = $(this).parent().find(".glyphicon");

        $(".panel-heading .glyphicon-minus-sign").not(icon).removeClass("glyphicon-minus-sign").addClass("glyphicon-plus-sign");

        if (icon.hasClass("glyphicon-plus-sign")) {
            icon.removeClass("glyphicon-plus-sign").addClass("glyphicon-minus-sign");
        } else {
            icon.removeClass("glyphicon-minus-sign").addClass("glyphicon-plus-sign");
        }
    });

    $.pnotify.defaults.history = false;
	$.pnotify.defaults.delay = 4500;

	if ($("#frmLoginHome").length == 0) {
		$(document).ajaxStart(function () {
			$.blockUI({
				message: "Aguarde...",
				css: {
					border: 'none',
					padding: '15px',
					backgroundColor: '#000',
					'-webkit-border-radius': '10px',
					'-moz-border-radius': '10px',
					opacity: .5,
					color: '#fff'
				}
			});
		});

		$(document).ajaxComplete(function (event, xhr, settings) {

			if (xhr.responseJSON && xhr.responseJSON.Status == "Error") {
				ShowMessage(xhr.responseJSON, 5000);
			}

			$.unblockUI();
		});

		$(document).ajaxError(function (event, jqxhr, settings, exception) {
			$("html").unblock();
		});
	}

	$("#Menu").click(function () {
	    if ($("#MenuItems").css("display") == "none") {
	        $("#MenuItems").fadeIn("fast");
	    } else {
	        $("#MenuItems").fadeOut("fast");
	    }
	});
});

function ShowMessage(json, duration) {
	if (json.Status == "Error") {
		$.pnotify({
			type: "error",
			title: "Erro",
			text: json.Message
		});
	}
	else if (json.Status == "Success") {
		$.pnotify({
			type: "success",
			title: "Sucesso",
			text: json.Message
		});
	}
}

function HideMessage() {
	var divMessage = $("#message");

	divMessage.slideUp("slow", function () {
		divMessage.addClass("hidden");
		divMessage.removeClass("alert-success");
		divMessage.removeClass("alert-block");
		divMessage.removeClass("alert-info");
		divMessage.removeClass("alert-error");
	});
}