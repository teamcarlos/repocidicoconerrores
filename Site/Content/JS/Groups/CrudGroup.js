﻿$(document).ready(function () {

    $("#Description").focus();
    
    if ($("#Id").val() > 0) {
        $("#accessList tbody tr:not(:hidden)").find("#btnRemoveAccess").on("click", function () {
            var row = $(this).parent().parent();
            $("#listMenus option[value=" + row.find("#MenuId").val() + "]").removeClass("hidden").prop("disabled", false);
            row.remove();
        });
    }

    $("#btnBack").click(function () {
        window.location = root + "Grupos/";
    });

    $("#frmGroup").submit(function () {
        return false;
    });

    $("#btnSave").click(function () {

        if (!$("#Description").val()) {
            ShowMessage({ Status: "Error", Message: "O campo Descrição é obrigatório." }, 5000);
            return false;
        }

        var menus = [];

        $.each($("#accessList tbody tr:not(:first)"), function (idx, item) {
            var isChecked = $(item).find("#Status option:selected").text() == "Ativo";
            var menuId = $(item).find("#MenuId").val();

            menus.push({ IsChecked: isChecked, MenuId: menuId });
        });

        var group = {
            Id: $("#Id").val(),
            Description: $("#Description").val(),
            Status: $("#Status option:selected").val(),
            MenuIds: menus
        };

        var url = $("#frmGroup").attr("action");

        $.ajaxSetup({
            dataType: 'json',
            contentType: 'application/json, charset=utf-8'
        });

        $.post(url, JSON.stringify(group), function (data) {
            ShowMessage(data, 5000);

            if (data.Status == "Success") {
                if ($("#Id").val() == 0) {
                    $("#Id").val(data.Id);
                    $("#title").text("Editar Grupo");
                    $("#breadCrumb").text("Editar Grupo");
                    $("#frmGroup").attr("action", data.FormAction);
                }
            }
        });
    });

    $("#btnAddMenu").on("click", function () {

        if ($("#listMenus option:selected").val() > 0) {

            var divSample = $("#accessList tbody tr:hidden").clone();
            divSample.find("#MenuId").val($("#listMenus").val());
            divSample.find("#menuName").text($("#listMenus option:selected").text());

            $("#listMenus option:selected").addClass("hidden").prop("disabled", true);
            $("#listMenus option").eq(0).prop("selected", true);

            divSample.find("#btnRemoveAccess").on("click", function () {
                $("#listMenus option[value=" + divSample.find("#MenuId").val() + "]").removeClass("hidden").prop("disabled", false);
                divSample.remove();
            });

            $("#accessList tbody").append(divSample);

            divSample.removeClass("hidden");
            
        }
        
    });
});