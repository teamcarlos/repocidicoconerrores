﻿$(document).ready(function () {

    $("input[type='text']").keydown(function (event) {
        if (event.which == 13) {
            $("#btnSearch").click();
        }
    });

    $("#btnNew").click(function () {
        var url = root + "Pacientes/Novo";
        window.location.href = url;
    });

    $("#btnSearch").click(function () {

        var url = root + "Pacientes/PesquisarPacientes";
        var searchTerms = GetSearchTerms();

        searchTerms.Page = 1;

        Paginate(url, searchTerms, "#results", GetSearchTerms);
    });
    
    $("#btnView, #btnEdit").click(function () {
        if (!$(this).hasClass("disabled")) {
            window.location = root + "Pacientes/EditarPaciente/" + $("#results input[type=checkbox]:checked").val();
        }

    });
    
    $("#btnDelete").click(function () {
        if (!$(this).hasClass("disabled")) {

            $.each($("#searchResult table tbody input[type=checkbox]:checked"), function (idx, item) {

                var url = root + "Pacientes/ExcluirPaciente/" + $(item).val();

                $.get(url, null, function (data) {
                    ShowMessage(data, 5000);

                    if (data.Status == "Success") {
                        $(item).parent().parent().remove();
                    }
                });
            });
        }
    });

});

function GetSearchTerms() {
    var searchTerms = {
        Id: $("#Id").val(),
        FirstName: $("#FirstName").val(),
        LastName: $("#LastName").val(),
        CPF: $("#CPF").val()
    };

    return searchTerms;
}