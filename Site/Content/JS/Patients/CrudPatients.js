﻿$(document).ready(function () {

    $("#FirstName").focus();
    
    $("#CPF").mask("999.999.999-99");
    $("#BirthDate").mask("99/99/9999");
    $("#Cellphone").mask("(99) 9999-9999");
    $("#HomePhone").mask("(99) 9999-9999");
    $("#ZipCode").mask("99999-999");


    $("#btnBack").click(function () {
        window.location = root + "Pacientes/";
    });

    $("#Email").change(function () {
        var value = $(this).val();
        if (!ValidateEmail(value)) {
            ShowMessage({ Status: "Error", Message: "E-mail inválido." }, 5000);
        } else {

        }
    });

    $("#btnSave").click(function () {
        var url = $("#frmPatient").attr("action");

        var patient = {
            Id: $("#Id").val(),
            FirstName: $("#FirstName").val(),
            LastName: $("#LastName").val(),
            CPF: $("#CPF").val(),
            RG: $("#RG").val(),
            BirthDate: $("#BirthDate").val(),
            Sex: $("#Sex option:selected").val(),
            Email: $("#Email").val(),
            Street: $("#Street").val(),
            Number: $("#Number").val(),
            Complement: $("#Complement").val(),
            District: $("#District").val(),
            City: $("#City").val(),
            State: $("#State").val(),
            ZipCode: $("#ZipCode").val(),
            Cellphone: $("#Cellphone").val(),
            HomePhone: $("#HomePhone").val()
        };

        $.ajaxSetup({
            dataType: "json",
            contentType: "application/json, charset=utf-8"
        });

        $.post(url, JSON.stringify(patient), function (data) {
            ShowMessage(data, 5000);
            
            if (data.Status == "Success") {
                
                if ($("#Id").val() == 0) {
                    ClearFields("#FirstName");
                } else {
                    
                }
            }
        });
    });
    
    $("#btnDelete").click(function () {
        if (!$(this).hasClass("disabled")) {

            var url = root + "Pacientes/ExcluirPaciente/" + $("#Id").val();

            $.get(url, null, function (data) {
                ShowMessage(data, 5000);
            });
        }
    });
});