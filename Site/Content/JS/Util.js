﻿function ValidateCPF(cpf) {

    cpf = cpf.replace(/[^\d]+/g, '');

    if (cpf == '') return false;

    // Elimina CPFs invalidos conhecidos
    if (cpf.length != 11 ||
        cpf == "00000000000" ||
        cpf == "11111111111" ||
        cpf == "22222222222" ||
        cpf == "33333333333" ||
        cpf == "44444444444" ||
        cpf == "55555555555" ||
        cpf == "66666666666" ||
        cpf == "77777777777" ||
        cpf == "88888888888" ||
        cpf == "99999999999")
        return false;

    // Valida 1o digito
    add = 0;
    for (i = 0; i < 9; i++)
        add += parseInt(cpf.charAt(i)) * (10 - i);
    rev = 11 - (add % 11);
    if (rev == 10 || rev == 11)
        rev = 0;
    if (rev != parseInt(cpf.charAt(9)))
        return false;

    // Valida 2o digito
    add = 0;
    for (i = 0; i < 10; i++)
        add += parseInt(cpf.charAt(i)) * (11 - i);
    rev = 11 - (add % 11);
    if (rev == 10 || rev == 11)
        rev = 0;
    if (rev != parseInt(cpf.charAt(10)))
        return false;

    return true;
}

function ValidateEmail(email) {
    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
}

function Paginate(url, searchTerms, resultDiv, getSearchTerms) {

    $.get(url, searchTerms, function (data) {
        
        $(resultDiv).html("");
        $("#paging-top").html("");
        $("#paging-bottom").html("");
        $("#paging-bottom-mobile").html("");

        $("#paging-top").html($(data.Pager).filter(".pagination"));
        $("#paging-bottom").html($(data.Pager).filter(".pagination"));
        $("#paging-bottom-mobile").html($(data.Pager).filter(".pager"));

        $(resultDiv).html("");
        $(resultDiv).html(data.SearchResult);

        $(".pagination li a").on("click", function(event) {

            if ($(this).attr("href") != "#") {

                // click no paginador via números - desktop ou tablet.
                var searchTerms = getSearchTerms();

                var itemText = $(this).text();
                var currentPage = parseInt($($(".pagination li.active a")[0]).text());

                if (isNaN(itemText)) {
                    if (itemText == "»") {
                        // próxima página
                        searchTerms.Page = currentPage + 1;
                    } else {
                        // página anterior
                        searchTerms.Page = currentPage - 1;
                    }
                } else {
                    searchTerms.Page = parseInt(itemText);
                }

                Paginate(url, searchTerms, resultDiv, getSearchTerms);

                return false;
            }

            return false;
        });

        $(".pager li a").on("click", function (event) {

            if ($(this).attr("href") != "#") {

                // click no paginador via palavra - mobiles.
                var searchTerms = getSearchTerms();
                var itemText = $(this).text();

                if (itemText == "Próxima") {
                    // próxima página
                    searchTerms.Page = parseInt($(this).parent().find("#NextPageValue").val()); 
                } else {
                    // página anterior
                    searchTerms.Page = parseInt($(this).parent().find("#PreviousPageValue").val());
                }

                Paginate(url, searchTerms, resultDiv, getSearchTerms);

                return false;
            }

            return false;
        });

        $(resultDiv).fadeIn("slow");

        ConfigureCheckBoxesSearchResult();
    });
}

function ConfigureCheckBoxesSearchResult() {

    $("#results").on("click", "tr:not(:first)", function () {

        if (!$(this).hasClass("selectedRow")) {
            $(this).addClass("selectedRow");
        } else {
            $(this).removeClass("selectedRow");
        }

        if ($("#results .selectedRow").length == 1) {
            EnableActionButtons(true, true);
        }
        else if ($("#results .selectedRow").length > 1) {
            EnableActionButtons(false, true);
        } else {
            EnableActionButtons(false, false);
        }
    });
}

function EnableActionButtons(enabled, enableDeleteButton) {

    if (enabled) {
        $("#btnView").addClass("active").removeClass("disabled");
        $("#btnEdit").addClass("active").removeClass("disabled");
    } else {
        $("#btnView").addClass("disabled").removeClass("active");
        $("#btnEdit").addClass("disabled").removeClass("active");
    }

    if (enableDeleteButton) {
        $("#btnDelete").removeClass("disabled").addClass("active");
    } else {
        $("#btnDelete").removeClass("active").addClass("disabled");
    }
}

function ClearFields(inputFocus) {

    $("#Id").val(0);
    
    $("input[type=text], input[type=password]").val("");
    $("select option:not(:first)").removeAttr("selected");
    $("select option:disabled").removeAttr("disabled");
    $("select option:first").attr("selected", "selected");

    $(inputFocus).focus();
}