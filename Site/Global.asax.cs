﻿using System;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Routing;
using Autofac;
using Autofac.Integration.Mvc;
using NeuroCloud.UI.Site.AppStart;
using NHibernate;
using NeuroCloud.Application;
using NeuroCloud.Application.Implementations;
using NeuroCloud.Application.SearchServices.Implementation;
using NeuroCloud.Infrastructure.Repositories.Config;
using Newtonsoft.Json;
using Site;

namespace NeuroCloud.UI.Site
{
	// Note: For instructions on enabling IIS6 or IIS7 classic mode, 
	// visit http://go.microsoft.com/?LinkId=9394801
	public class MvcApplication : System.Web.HttpApplication
	{
		protected void Application_Start()
		{
			AreaRegistration.RegisterAllAreas();

			WebApiConfig.Register(GlobalConfiguration.Configuration);
			FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
			RouteConfig.RegisterRoutes(RouteTable.Routes);

			NHInitializer.Init();

            var builder = new ContainerBuilder();
            
            builder.RegisterControllers(typeof(MvcApplication).Assembly);
            builder.RegisterAssemblyModules(typeof(NHInitializer).Assembly);

		    builder.RegisterAssemblyTypes(typeof(UserSignUp).Assembly).AsImplementedInterfaces();
		    builder.RegisterAssemblyTypes(typeof (SearchServiceBase).Assembly).AsImplementedInterfaces();

            var container = builder.Build();
            DependencyResolver.SetResolver(new AutofacDependencyResolver(container));
            
            HibernatingRhinos.Profiler.Appender.NHibernate.NHibernateProfiler.Initialize();
		}

		void Application_Error(object sender, EventArgs e)
		{
			var exception = Server.GetLastError();
			var isAjaxCall = string.Equals("XMLHttpRequest", Context.Request.Headers["x-requested-with"], StringComparison.OrdinalIgnoreCase);

			if (isAjaxCall)
			{
				Context.ClearError();
				Context.Response.ContentType = "application/json";
				Context.Response.StatusCode = 200;

				if (exception is HibernateException)
				{
					var jsonResult = new
						                 {
											 Status = "Error", 
											 Message = "Ocorreu um erro ao tentar executar a operação. Verifique os dados e tente novamente."
						                 };
					Context.Response.Write(JsonConvert.SerializeObject(jsonResult, Formatting.Indented));
				}
			}
		}
	}
}