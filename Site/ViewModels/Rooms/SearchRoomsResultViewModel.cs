﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NeuroCloud.UI.Site.ViewModels.Rooms
{
	public class SearchRoomsResultViewModel
	{
		public long Id { get; set; }
		public string Description { get; set; }
		public string SectorDescription { get; set; }
		public string RoomType { get; set; }
	}
}