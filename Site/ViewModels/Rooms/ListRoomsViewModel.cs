﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace NeuroCloud.UI.Site.ViewModels.Rooms
{
	public class ListRoomsViewModel
	{
		public ListRoomsViewModel()
		{
			this.Sectors = new List<KeyValueViewModel<long>>();
		}

		public long SectorId { get; set; }
		public string Description { get; set; }

		public IList<KeyValueViewModel<long>> Sectors { get; set; }

		public int Page { get; set; }
	}
}