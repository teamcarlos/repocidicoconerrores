﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using NeuroCloud.Domain.Enums;

namespace NeuroCloud.UI.Site.ViewModels.Rooms
{
	public class CrudRoomViewModel
	{
		public CrudRoomViewModel()
		{
			this.Sectors = new List<KeyValueViewModel<long>>();
			this.Beds= new List<KeyValueViewModel<long>>();
		}

		public long Id { get; set; }
		public string Description { get; set; }
		public long SectorId { get; set; }
		public int MaximumNumberOfPatients { get; set; }
		public int RoomType { get; set; }

		public IList<KeyValueViewModel<long>> Sectors { get; set; }

		public IList<KeyValueViewModel<long>> Beds { get; set; }
	}
}