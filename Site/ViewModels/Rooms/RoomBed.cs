﻿namespace NeuroCloud.UI.Site.ViewModels.Rooms
{
	public class RoomBed
	{
		public long Id { get; set; }
		public string Description { get; set; }
	}
}