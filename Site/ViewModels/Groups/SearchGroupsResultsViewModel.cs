﻿using System;

namespace NeuroCloud.UI.Site.ViewModels.Groups
{
    public class SearchGroupsResultsViewModel
    {
        public Int64 Id { get; set; }
        public string Description { get; set; }
    }
}