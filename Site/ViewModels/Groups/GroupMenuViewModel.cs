﻿namespace NeuroCloud.UI.Site.ViewModels.Groups
{
	public class GroupMenuViewModel
	{
		public long MenuId { get; set; }
		public string MenuDescription { get; set; }
		public bool IsChecked { get; set; }
	}
}