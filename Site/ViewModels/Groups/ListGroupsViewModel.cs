﻿namespace NeuroCloud.UI.Site.ViewModels.Groups
{
    public class ListGroupsViewModel
    {
        public long Id { get; set; }

        public string Name { get; set; }

        public int Page { get; set; }
    }
}