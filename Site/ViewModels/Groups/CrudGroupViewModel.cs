﻿using System.Collections.Generic;
using System.Web.Mvc;

namespace NeuroCloud.UI.Site.ViewModels.Groups
{
    public class CrudGroupViewModel
    {
	    public CrudGroupViewModel()
	    {
    		this.MenuList = new List<KeyValueViewModel<long>>();
			this.MenuIds = new List<GroupMenuViewModel>();
	    }

        public long Id { get; set; }
        public string Description { get; set; }
        public int Status { get; set; }

	    public IList<KeyValueViewModel<long>> MenuList { get; set; }

        public IList<GroupMenuViewModel> MenuIds { get; set; }
    }
}