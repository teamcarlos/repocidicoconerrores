﻿namespace NeuroCloud.UI.Site.ViewModels
{
    public class SectorViewModel
    {
        public long Id { get; set; }
        public string Description { get; set; }
    }
}