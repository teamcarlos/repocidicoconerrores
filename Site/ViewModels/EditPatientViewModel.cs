﻿using System;

namespace NeuroCloud.UI.Site.ViewModels
{
	public class EditPatientViewModel
	{
		public long Id { get; set; }

		public string FirstName { get; set; }
		public string LastName { get; set; }
		public string CPF { get; set; }
		public string RG { get; set; }
		public DateTime? BirthDate { get; set; }
		public string Email { get; set; }
		public int Sex { get; set; }

		public string Street { get; set; }
		public string Number { get; set; }
		public string Complement { get; set; }
		public string District { get; set; }
		public string City { get; set; }
		public string State { get; set; }
		public string ZipCode { get; set; }

		public string Cellphone { get; set; }
		public string HomePhone { get; set; }
	}
}