namespace NeuroCloud.UI.Site.ViewModels.Patients
{
    public class SearchPatientsResultViewModel
    {
        public long Id { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string CPF { get; set; }
    }
}