﻿using System.Collections.Generic;

namespace NeuroCloud.UI.Site.ViewModels.Users
{
    public class UserSignedInViewModel
    {
        public UserSignedInViewModel()
        {
            this.UserMenus = new List<UserMenuViewModel>();
        }

        public long Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }

        public IList<UserMenuViewModel> UserMenus{ get; set; }

    }

    public class UserMenuViewModel
    {
        public UserMenuViewModel()
        {
            this.Features = new List<FeaturesViewModel>();
        }

        public long Id { get; set; }
        public string Description { get; set; }
        public int Sequence { get; set; }
	    public bool DisplayOnScreen { get; set; }

        public IList<FeaturesViewModel> Features { get; set; }
    }

    public class FeaturesViewModel
    {
        public long Id { get; set; }

        public string Description { get; set; }

        public int Sequence { get; set; }

        public string Url { get; set; }

	    public bool DisplayOnScreen { get; set; }
    }
}