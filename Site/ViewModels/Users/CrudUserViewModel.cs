﻿using System;
using System.Collections.Generic;

namespace NeuroCloud.UI.Site.ViewModels.Users
{
	public class CrudUserViewModel
	{
		public CrudUserViewModel()
		{
			this.Sectors = new List<KeyValueViewModel<long>>();
			this.Groups = new List<KeyValueViewModel<long>>();
            this.GroupsIds = new List<KeyValueViewModel<long>>();
		}

	    public Int64 Id { get; set; }
		public string FirstName { get; set; }
		public string LastName { get; set; }
		public string CPF { get; set; }
		public string RG { get; set; }
		public DateTime? BirthDate { get; set; }
		public string Email { get; set; }
		public string Record { get; set; }
		public string Login { get; set; }
		public string Password { get; set; }
		public Int64 SectorId { get; set; }

		public IList<KeyValueViewModel<long>> Sectors { get; set; }
		public IList<KeyValueViewModel<long>> Groups { get; set; }
        public IList<KeyValueViewModel<long>> GroupsIds { get; set; }

		
	}
}
