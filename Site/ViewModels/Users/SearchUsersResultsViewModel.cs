﻿using System;

namespace NeuroCloud.UI.Site.ViewModels.Users
{
	public class SearchUsersResultsViewModel
	{
		public Int64 Id { get; set; }
		public string FirstName { get; set; }
		public string LastName { get; set; }
		public string CPF { get; set; }
		public string Record { get; set; }
	}
}