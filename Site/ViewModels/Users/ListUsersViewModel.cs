﻿using System;
using System.Collections.Generic;

namespace NeuroCloud.UI.Site.ViewModels.Users
{
    public class ListUsersViewModel
    {
        public ListUsersViewModel()
        {
            this.Sectors = new List<KeyValueViewModel<long>>();
        }

        public string Name { get; set; }
        public Int64 SectorId { get; set; }
        public int Page { get; set; }

        public IList<KeyValueViewModel<long>> Sectors { get; set; }
    }
}