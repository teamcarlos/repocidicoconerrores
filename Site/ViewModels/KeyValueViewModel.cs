﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NeuroCloud.UI.Site.ViewModels
{
    public class KeyValueViewModel<T>
    {
        public T Id { get; set; }
        public string Value { get; set; }
    }
}