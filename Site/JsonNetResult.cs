﻿using System;
using System.Web.Mvc;
using Newtonsoft.Json;

namespace NeuroCloud.UI.Site
{
	public class JsonNetResult : JsonResult
	{
		public override void ExecuteResult(ControllerContext context)
		{
			if (context == null)
				throw new ArgumentNullException("context");

			var response = context.HttpContext.Response;

			response.ContentType = !string.IsNullOrEmpty(ContentType) ? ContentType : "application/json";

			if (ContentEncoding != null)
				response.ContentEncoding = ContentEncoding;

			if (Data == null)
				return;

			// If you need special handling, you can call another form of SerializeObject below
			var serializedObject = JsonConvert.SerializeObject(Data, Formatting.Indented);
			response.Write(serializedObject);
		}
	}
}