﻿using System;
using System.Linq;
using System.Web.Mvc;
using NeuroCloud.Application;
using NeuroCloud.Application.SearchServices;
using NeuroCloud.Domain.Components;
using NeuroCloud.Domain.Entities;
using NeuroCloud.Domain.Enums;
using NeuroCloud.Domain.QueryObjects;
using NeuroCloud.UI.Site.ViewModels.Patients;

namespace NeuroCloud.UI.Site.Controllers
{
    public class PacientesController : BaseController
    {
	    private ISearchPatientsService SearchPatientsService { get; set; }
	    private IPatientCrudService PatientCrudService { get; set; }

	    public PacientesController(ISearchPatientsService searchPatientsService, IPatientCrudService patientCrudService)
	    {
		    this.SearchPatientsService = searchPatientsService;
		    this.PatientCrudService = patientCrudService;
	    }

        [HttpGet, OutputCache(Duration = 1, NoStore = true)]
        public ActionResult Index()
        {
            return View("ListPatients");
        }

        [HttpGet, OutputCache(Duration = 1, NoStore = true)]
		public ActionResult ListarPacientes()
		{
            return View("ListPatients");
		}

        [HttpGet, OutputCache(Duration = 1, NoStore = true)]
		public ActionResult Novo()
        {
            var viewModel = new CrudPatientViewModel();
			return View("CrudPatient",viewModel);
		}

        [HttpPost, OutputCache(Duration = 1, NoStore = true)]
	    public JsonResult NovoPaciente(CrudPatientViewModel viewModel)
	    {
		    var patient = new Patient
			                  {
				                  FirstName = viewModel.FirstName,
				                  LastName = viewModel.LastName,
				                  CPF = viewModel.CPF,
				                  RG = viewModel.RG,
				                  Sex = (Sex) viewModel.Sex,
				                  BirthDate =  Convert.ToDateTime(viewModel.BirthDate),
				                  Email = viewModel.Email,
				                  HomePhone = viewModel.HomePhone,
				                  CellPhone = viewModel.Cellphone,
				                  CreatedAt = DateTime.Now,
				                  Address = new Address
					                            {
						                            Street = viewModel.Street,
						                            Number = viewModel.Number,
						                            Complement = viewModel.Complement,
						                            District = viewModel.District,
						                            City = viewModel.City,
						                            State = viewModel.State,
						                            ZipCode = viewModel.ZipCode
					                            }
			                  };

		    try
		    {
				this.PatientCrudService.SaveNewPatient(patient);

                return Json(new { Status = "Success", Message = "Paciente salvo com sucesso.", Id = patient.Id, FormAction = Url.Action("SalvarPaciente", "Pacientes") }, JsonRequestBehavior.DenyGet);
		    }
		    catch (Exception exception)
		    {
				return this.ErrorResult(exception.Message, JsonRequestBehavior.DenyGet);
		    }
	    }

        [HttpGet, OutputCache(Duration = 1, NoStore = true)]
	    public ActionResult EditarPaciente(long id)
	    {
		    var patient = this.SearchPatientsService.GetById(id);
		    
			var viewModel = new CrudPatientViewModel
			                    {
									Id = patient.Id,
				                    FirstName = patient.FirstName,
				                    LastName = patient.LastName,
				                    CPF = patient.CPF,
				                    RG = patient.RG,
				                    BirthDate = patient.BirthDate,
				                    Sex = (int) patient.Sex,
				                    Email = patient.Email,
				                    Street = patient.Address.Street,
				                    Number = patient.Address.Number,
				                    Complement = patient.Address.Complement,
				                    District = patient.Address.District,
				                    City = patient.Address.City,
				                    State = patient.Address.State,
				                    ZipCode = patient.Address.ZipCode,
				                    Cellphone = patient.CellPhone,
				                    HomePhone = patient.HomePhone
			                    };


			return View("CrudPatient", viewModel);
		}

        [HttpPost, OutputCache(Duration = 1, NoStore = true)]
		public JsonResult SalvarPaciente(CrudPatientViewModel viewModel)
		{
			var patient = new Patient
			{
				Id = viewModel.Id,
				FirstName = viewModel.FirstName,
				LastName = viewModel.LastName,
				CPF = viewModel.CPF,
				RG = viewModel.RG,
				Sex = (Sex)viewModel.Sex,
				BirthDate = Convert.ToDateTime(viewModel.BirthDate),
				Email = viewModel.Email,
				HomePhone = viewModel.HomePhone,
				CellPhone = viewModel.Cellphone,
				CreatedAt = DateTime.Now,
				Address = new Address
				{
					Street = viewModel.Street,
					Number = viewModel.Number,
					Complement = viewModel.Complement,
					District = viewModel.District,
					City = viewModel.City,
					State = viewModel.State,
					ZipCode = viewModel.ZipCode
				}
			};

			try
			{
				this.PatientCrudService.UpdatePatient(patient);

                return Json(new { Status = "Success", Message = "Paciente atualizado com sucesso.", Id = patient.Id, FormAction = Url.Action("SalvarPaciente", "Pacientes") }, JsonRequestBehavior.DenyGet);
			}
			catch (Exception exception)
			{
				return this.ErrorResult(exception.Message, JsonRequestBehavior.DenyGet);
			}
		}

        [HttpGet, OutputCache(Duration = 1, NoStore = true)]
        public JsonResult PesquisarPacientes(SearchPatientsViewModel viewModel)
		{
			var queryObject = new SearchPatientQueryObject
				                  {
					                  Id = viewModel.Id,
					                  FirstName = viewModel.FirstName,
					                  LastName = viewModel.LastName,
					                  CPF = viewModel.CPF,
					                  CurrentPage = viewModel.Page
				                  };

            var patients = this.SearchPatientsService.SearchPatients(queryObject);

            var result = patients.Select(patient => new SearchPatientsResultViewModel
            {
                Id = patient.Id, FirstName = patient.FirstName, LastName = patient.LastName, CPF = patient.CPF
            }).ToList();

			ViewData["QueryObject"] = queryObject;

            return Json(new
            {
                SearchResult = RenderPartialView(this, "_SearchPatientsResult", result),
                Pager = RenderPartialView(this, "_Pager", queryObject)
            }, JsonRequestBehavior.AllowGet);
        }

        [HttpGet, OutputCache(Duration = 1, NoStore = true)]
        public JsonResult ExcluirPaciente(long id)
        {
            Patient patient = null;

            try
            {
                patient = this.SearchPatientsService.GetById(id);
                this.PatientCrudService.InactivatePatient(patient);
            }
            catch (InvalidOperationException invalidOperationException)
            {
                return this.ErrorResult(invalidOperationException.Message, JsonRequestBehavior.DenyGet);
            }

            if (patient != null && patient.Status == Status.Inactive)
            {
                return Json(new { Status = "Success", Message = string.Format("Paciente '{0} {1}' excluído com sucesso.", patient.FirstName, patient.LastName) }, JsonRequestBehavior.DenyGet);
            }

            return Json(new { Status = "Error", Message = string.Format("Ocorreu um erro ao excluir o paciente '{0} {1}'.", patient.FirstName, patient.LastName) }, JsonRequestBehavior.DenyGet);
        }
    }
}
