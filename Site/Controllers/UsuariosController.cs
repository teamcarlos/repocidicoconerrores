﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using NeuroCloud.Application;
using NeuroCloud.Application.SearchServices;
using NeuroCloud.Domain.Entities;
using NeuroCloud.Domain.Enums;
using NeuroCloud.Domain.QueryObjects;
using NeuroCloud.UI.Site.ViewModels;
using NeuroCloud.UI.Site.ViewModels.Groups;
using NeuroCloud.UI.Site.ViewModels.Users;

namespace NeuroCloud.UI.Site.Controllers
{
    public class UsuariosController : BaseController
    {
        private IUserSignUpService UserSignUpService { get; set; }
        private ISearchUsersService SearchUsersService { get; set; }
        private ISearchSectorsService SearchSectorsService { get; set; }
        private ISearchGroupsService SearchGroupsService { get; set; }

        public UsuariosController(IUserSignUpService userSignUpService, ISearchUsersService searchUsersService, ISearchSectorsService searchSectorsService, ISearchGroupsService searchGroupsService)
        {
            this.UserSignUpService = userSignUpService;
            this.SearchUsersService = searchUsersService;
            this.SearchSectorsService = searchSectorsService;
            this.SearchGroupsService = searchGroupsService;
        }

        [HttpGet, OutputCache(Duration = 1, NoStore = true)]
        public ActionResult Index()
        {
            return View();
        }

        [HttpGet, OutputCache(Duration = 1, NoStore = true)]
        public ActionResult Novo()
        {
            var viewModel = new CrudUserViewModel();

            var sectors = this.SearchSectorsService.GetSectors();
            var groups = this.SearchGroupsService.GetAll();

            sectors.ToList().ForEach(x => viewModel.Sectors.Add(new KeyValueViewModel<long> { Value = x.Description, Id = x.Id }));
            groups.ToList().ForEach(x => viewModel.Groups.Add(new KeyValueViewModel<long> { Value = x.Description, Id = x.Id }));

            return View("CrudUser", viewModel);
        }

        [HttpGet, OutputCache(Duration = 1, NoStore = true)]
        public ActionResult ListarUsuarios()
        {
            var sectors = this.SearchSectorsService.GetSectors();
            var viewModel = new ListUsersViewModel();
            sectors.ToList().ForEach(x => viewModel.Sectors.Add(new KeyValueViewModel<long> { Value = x.Description, Id = x.Id }));

            return View("ListUsers", viewModel);
        }

        [HttpGet, OutputCache(Duration = 1, NoStore = true)]
        public JsonResult PesquisarUsuarios(ListUsersViewModel viewModel)
        {
            var queryObject = new SearchUserQueryObject
            {
                Name = viewModel.Name,
                SectorId = viewModel.SectorId,
                CurrentPage = viewModel.Page
            };

            var result = this.SearchUsersService.SearchUsers(queryObject);

            var viewModelResult =
                result.Select(x => new SearchUsersResultsViewModel()
                {
                    Id = x.Id,
                    CPF = x.CPF,
                    FirstName = x.FirstName,
                    LastName = x.LastName,
                    Record = x.Record
                }).ToArray();

            ViewData["QueryObject"] = queryObject;

            return Json(new
            {
                SearchResult = RenderPartialView(this, "_SearchUsersResult", viewModelResult),
                Pager = RenderPartialView(this, "_Pager", queryObject)
            }, JsonRequestBehavior.AllowGet);
        }

        [HttpGet, OutputCache(Duration = 1, NoStore = true)]
        public ActionResult EditarUsuario(Int64 id)
        {
            var sectors = this.SearchSectorsService.GetSectors();
            var groups = this.SearchGroupsService.GetAll();
            var user = this.SearchUsersService.GetById(id);

            var userViewModel = new CrudUserViewModel()
            {
                Id = user.Id,
                FirstName = user.FirstName,
                LastName = user.LastName,
                BirthDate = user.BirthDate,
                CPF = user.CPF,
                Email = user.Email,
                Login = user.Username,
                RG = user.RG,
                Record = user.Record,
                Password = user.Password
            };

            sectors.ToList().ForEach(x => userViewModel.Sectors.Add(new KeyValueViewModel<long> { Value = x.Description, Id = x.Id }));
            groups.ToList().ForEach(x => userViewModel.Groups.Add(new KeyValueViewModel<long> { Value = x.Description, Id = x.Id }));

            userViewModel.SectorId = user.Sector.Id;
            user.Groups.ToList().ForEach(x => userViewModel.GroupsIds.Add(new KeyValueViewModel<long> { Id = x.Group.Id }));

            return View("CrudUser", userViewModel);
        }

        [HttpPost, OutputCache(Duration = 1, NoStore = true)]
        public JsonResult NovoUsuario(CrudUserViewModel viewModel)
        {
            var sectors = this.SearchSectorsService.GetSectors();

            var user = new User
                           {
                               FirstName = viewModel.FirstName,
                               LastName = viewModel.LastName,
                               CPF = viewModel.CPF,
                               RG = viewModel.RG,
                               Email = viewModel.Email,
                               CreatedAt = DateTime.Now,
                               Username = viewModel.Login,
                               Password = viewModel.Password,
                               Record = viewModel.Record,
                               Sector = sectors.FirstOrDefault(x => x.Id == viewModel.SectorId)
                           };

            if (viewModel.BirthDate.HasValue) user.BirthDate = viewModel.BirthDate.Value;

            try
            {
                this.UserSignUpService.SaveNewUser(user);

                return Json(new { Status = "Success", Message = "Usuário salvo com sucesso.", Id=user.Id, FormAction = Url.Action("SalvarUsuario", "Usuarios") }, JsonRequestBehavior.DenyGet);
            }
            catch (InvalidOperationException invalidOperationException)
            {
                return this.ErrorResult(invalidOperationException.Message, JsonRequestBehavior.DenyGet);
            }
        }

        [HttpPost, OutputCache(Duration = 1, NoStore = true)]
        public JsonResult SalvarUsuario(CrudUserViewModel viewModel)
        {
            var groups = this.SearchGroupsService.GetAll();
            var sectors = this.SearchSectorsService.GetSectors();

            var user = this.SearchUsersService.GetById(viewModel.Id);

            user.FirstName = viewModel.FirstName;
            user.LastName = viewModel.LastName;
            user.CPF = viewModel.CPF;
            user.RG = viewModel.RG;
            user.Email = viewModel.Email;
            user.Record = viewModel.Record;
            user.Username = viewModel.Login;
            user.Password = string.IsNullOrEmpty(viewModel.Password) ? user.Password : viewModel.Password;
            user.Sector = sectors.FirstOrDefault(x => x.Id == viewModel.SectorId);

            user.Groups.Clear();

            foreach (var group in viewModel.GroupsIds)
            {
                user.Groups.Add(new UserGroup { Group = groups.FirstOrDefault(x => x.Id == group.Id), User = user });
            }

            if (viewModel.BirthDate.HasValue)
                user.BirthDate = viewModel.BirthDate.Value;

            try
            {
                this.UserSignUpService.UpdateUser(user);
            }
            catch (InvalidOperationException invalidOperationException)
            {
                return this.ErrorResult(invalidOperationException.Message, JsonRequestBehavior.DenyGet);
            }

            return Json(new { Status = "Success", Message = "Usuário alterado com sucesso." }, JsonRequestBehavior.DenyGet);
        }

        [HttpGet, OutputCache(Duration = 1, NoStore = true)]
        public JsonResult ExcluirUsuario(Int64 id)
        {
            User user = null;

            try
            {
                user = this.SearchUsersService.GetById(id);
                this.UserSignUpService.InactivateUser(user);
            }
            catch (InvalidOperationException invalidOperationException)
            {
                return this.ErrorResult(invalidOperationException.Message, JsonRequestBehavior.DenyGet);
            }

            if (user != null && user.Status == Status.Inactive)
            {
                return Json(new { Status = "Success", Message = string.Format("Usuário '{0} {1}' excluído com sucesso.", user.FirstName, user.LastName) }, JsonRequestBehavior.DenyGet);    
            }

            return Json(new { Status = "Error", Message = string.Format("Ocorreu um erro ao excluir o usuário '{0} {1}'.", user.FirstName, user.LastName) }, JsonRequestBehavior.DenyGet);    
        }

        [HttpGet, OutputCache(Duration = 1, NoStore = true)]
        public ActionResult Logoff()
        {
            Session.Clear();
            return RedirectToAction("Index", "Home");
        }
    }
}
