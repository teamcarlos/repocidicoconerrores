﻿using System;
using System.Linq;
using System.Web.Mvc;
using Iesi.Collections.Generic;
using NeuroCloud.Application;
using NeuroCloud.Application.SearchServices;
using NeuroCloud.Domain.Entities;
using NeuroCloud.Domain.Enums;
using NeuroCloud.Domain.QueryObjects;
using NeuroCloud.UI.Site.ViewModels;
using NeuroCloud.UI.Site.ViewModels.Groups;
using NHibernate.Mapping;
using GroupMenu = NeuroCloud.Domain.Entities.GroupMenu;

namespace NeuroCloud.UI.Site.Controllers
{
	public class GruposController : BaseController
	{
		private ISearchGroupsService SearchGroupsService { get; set; }
		private IGroupCrudService GroupCrudService { get; set; }
		private ISearchMenusService SearchMenusService { get; set; }

		public GruposController(ISearchGroupsService searchGroupsService, IGroupCrudService groupCrudService, ISearchMenusService searchMenusService)
		{
			this.SearchGroupsService = searchGroupsService;
			this.GroupCrudService = groupCrudService;
			this.SearchMenusService = searchMenusService;
		}

		[HttpGet , OutputCache(Duration = 1, NoStore = true)]
		public ActionResult Index()
		{
            return View("ListGroups");
		}

        [HttpGet, OutputCache(Duration = 1, NoStore = true)]
        public JsonResult PesquisarGrupos(ListGroupsViewModel viewModel)
        {
            var queryObject = new SearchUserQueryObject
            {
                Name = viewModel.Name,
                CurrentPage = viewModel.Page
            };

            var result = this.SearchGroupsService.SearchGroups(queryObject);

            var viewModelResult =
                result.Select(x => new SearchGroupsResultsViewModel
                {
                    Id = x.Id,
                    Description = x.Description
                }).ToArray();

            ViewData["QueryObject"] = queryObject;

            return Json(new
            {
                SearchResult = RenderPartialView(this, "_SearchGroupsResult", viewModelResult),
                Pager = RenderPartialView(this, "_Pager", queryObject)
            }, JsonRequestBehavior.AllowGet);
        }

		[HttpGet, OutputCache(Duration = 1, NoStore = true)]
		public ActionResult EditarGrupo(long id)
		{
			var group = this.SearchGroupsService.GetById(id);
			var viewModel = new CrudGroupViewModel
								{
									Id = group.Id,
									Description = group.Description,
									Status = (int)group.Status
								};

			var menus = this.SearchMenusService.GetAll();

            foreach (var menu in menus)
                viewModel.MenuList.Add(new KeyValueViewModel<long> { Value = menu.Description, Id = menu.Id });

		    foreach (var groupMenu in group.Menus)
		    {
		        viewModel.MenuIds.Add(new GroupMenuViewModel
		        {
		            MenuId = groupMenu.Menu.Id,
		            MenuDescription = groupMenu.Menu.Description,
		            IsChecked = groupMenu.Status == Status.Active
		        });
		    }
            
			return View("CrudGroup", viewModel);
		}

		[HttpGet, OutputCache(Duration = 1, NoStore = true)]
		public ActionResult Novo()
		{
			var viewModel = new CrudGroupViewModel();
			var menus = this.SearchMenusService.GetAll();

		    foreach (var menu in menus)
		        viewModel.MenuList.Add(new KeyValueViewModel<long> {Value = menu.Description, Id = menu.Id});

			return View("CrudGroup", viewModel);
		}

		[HttpPost, OutputCache(Duration = 1, NoStore = true)]
		public JsonResult NovoGrupo(CrudGroupViewModel viewModel)
		{
			if (string.IsNullOrEmpty(viewModel.Description))
				return Json(new { Status = "Error", Message = "O campo descrição é de preenchimento obrigatório." }, JsonRequestBehavior.DenyGet);

			if (viewModel.Status == 0)
				return Json(new { Status = "Error", Message = "O campo descrição é de preenchimento obrigatório." }, JsonRequestBehavior.DenyGet);

			var group = new Group();
			group.Description = viewModel.Description;

			if (viewModel.Status > 0)
				group.Status = (Status)viewModel.Status;

			foreach (var menu in viewModel.MenuIds)
			{
				group.Menus.Add(new GroupMenu
				{
					Menu = new Menu { Id = menu.MenuId },
					Status = menu.IsChecked ? Status.Active : Status.Inactive
				});
			}

			var result = this.GroupCrudService.SaveNewGroup(group);

			if (result.Id > 0)
				return Json(new { Status = "Success", Message = "Grupo inserido com sucesso." }, JsonRequestBehavior.DenyGet);

			return Json(new { Status = "Error", Message = "Não foi possível cadastrar o novo grupo. Verfique os dados e tente novamente." }, JsonRequestBehavior.DenyGet);
		}

		[HttpPost, OutputCache(Duration = 1, NoStore = true)]
		public JsonResult SalvarGrupo(CrudGroupViewModel viewModel)
		{
			var entity = this.SearchGroupsService.GetById(viewModel.Id);
			var menus = this.SearchMenusService.GetAll();

			entity.Description = viewModel.Description;
			entity.Status = (Status)viewModel.Status;
			entity.Menus.Clear();

			foreach (var menu in viewModel.MenuIds)
			{
				var menuSelected = menus.FirstOrDefault(x => x.Id == menu.MenuId);

				entity.Menus.Add(new GroupMenu
					                 {
										 Menu = menuSelected,
						                 Group = entity,
						                 Status = menu.IsChecked ? Status.Active : Status.Inactive
					                 });
			}

			try
			{
				this.GroupCrudService.UpdateGroup(entity);
			}
			catch (Exception exception)
			{
				return Json(new { Status = "Error", Message = "Não foi possível atualizar o novo grupo. Verfique os dados e tente novamente." }, JsonRequestBehavior.DenyGet);
			}

			return Json(new { Status = "Success", Message = "Grupo atualizado com sucesso." }, JsonRequestBehavior.DenyGet);
		}

        [HttpGet, OutputCache(Duration = 1, NoStore = true)]
        public JsonResult ExcluirGrupo(long id)
        {
            Group groupEntity = null;

            try
            {
                groupEntity = this.SearchGroupsService.GetById(id);
                this.GroupCrudService.InactivateGroup(groupEntity);
            }
            catch (InvalidOperationException invalidOperationException)
            {
                return this.ErrorResult(invalidOperationException.Message, JsonRequestBehavior.DenyGet);
            }

            if (groupEntity != null && groupEntity.Status == Status.Inactive)
            {
                return Json(new { Status = "Success", Message = string.Format("Grupo '{0}' excluído com sucesso.", groupEntity.Description) }, JsonRequestBehavior.DenyGet);
            }

            return Json(new { Status = "Error", Message = string.Format("Ocorreu um erro ao excluir o grupo '{0}'.", groupEntity.Description) }, JsonRequestBehavior.DenyGet);    
        }
	}
}
