﻿using System.IO;
using System.Text;
using System.Web.Mvc;
using NeuroCloud.UI.Site.ViewModels.Users;

namespace NeuroCloud.UI.Site.Controllers
{
    public abstract class BaseController : Controller
    {
        public UserSignedInViewModel UserSignedIn
        {
            get { return (UserSignedInViewModel) Session["UserSignedIn"]; }
            set { Session["UserSignedIn"] = value; }
        }

        protected JsonResult ErrorResult(string message, JsonRequestBehavior behavior = JsonRequestBehavior.AllowGet)
        {
            return new JsonNetResult
            {
                Data = new { Message = message, Status="Error"},
                ContentEncoding = Encoding.UTF8,
                JsonRequestBehavior = behavior
            };
        }

		protected JsonResult SuccessResult(string message, JsonRequestBehavior behavior = JsonRequestBehavior.AllowGet)
		{
			return new JsonNetResult
			{
				Data = new { Message = message, Status = "Success" },
				ContentEncoding = Encoding.UTF8,
				JsonRequestBehavior = behavior
			};
		}

        protected override JsonResult Json(object data, string contentType, Encoding contentEncoding, JsonRequestBehavior behavior)
        {
            return new JsonNetResult
            {
                Data = data,
                ContentType = contentType,
                ContentEncoding = contentEncoding,
                JsonRequestBehavior = behavior
            };
        }

        public static string RenderPartialView(Controller controller, string viewName, object model)
        {
            if (string.IsNullOrEmpty(viewName))
                viewName = controller.ControllerContext.RouteData.GetRequiredString("action");

            controller.ViewData.Model = model;
            using (var sw = new StringWriter())
            {
                ViewEngineResult viewResult = ViewEngines.Engines.FindPartialView(controller.ControllerContext, viewName);
                var viewContext = new ViewContext(controller.ControllerContext, viewResult.View, controller.ViewData, controller.TempData, sw);
                viewResult.View.Render(viewContext, sw);

                return sw.GetStringBuilder().ToString();
            }
        }

        protected override void OnAuthorization(AuthorizationContext filterContext)
        {
            if (this.UserSignedIn == null)
            {
                
                RedirectToAction("Login", "Home");
            }
        }

        protected override void EndExecute(System.IAsyncResult asyncResult)
        {
            ViewBag.User = this.UserSignedIn;
            
            if (this.UserSignedIn == null)
            {
                RedirectToAction("Login", "Home");
            }

           
            base.EndExecute(asyncResult);
        }
    }
}