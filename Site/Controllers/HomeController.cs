﻿using System.Linq;
using System.Web;
using System.Web.Mvc;
using NeuroCloud.Application;
using NeuroCloud.Domain.Entities;
using NeuroCloud.UI.Site.ViewModels.Users;

namespace NeuroCloud.UI.Site.Controllers
{
	public class HomeController : BaseController
	{
		private IUserSignInService UserSignInService { get; set; }

		public HomeController(IUserSignInService userSignInService)
		{
			UserSignInService = userSignInService;
		}

        [HttpGet, OutputCache(Duration = 1, NoStore = true)]
		public ActionResult Index()
		{
			if (base.UserSignedIn != null)
			{
				return RedirectToAction("Index", "Usuarios");
			}

			return View();
		}

        [HttpGet, OutputCache(Duration = 1, NoStore = true)]
		public ActionResult Inicial()
		{
			return RedirectToAction("Index");
		}

        [HttpGet, OutputCache(Duration = 1, NoStore = true)]
		public JsonResult Login(string username, string password)
		{
			var result = this.UserSignInService.SignIn(username, password);

			if (result == null)
			{
				return Json(new
								{
									Status = "Error",
									Message = "Login incorreto."
								}, JsonRequestBehavior.AllowGet);
			}

			// Listar perfil do usuário aqui

			var userSignedIn = new UserSignedInViewModel();

			userSignedIn.Id = result.Id;
			userSignedIn.FirstName = result.FirstName;
			userSignedIn.LastName = result.LastName;
            
			base.UserSignedIn = BuildUserMenus(result, userSignedIn);

			return Json(new
							{
								Status = "Success",
								Message = "Login efetuado com sucesso! Redirecionando...",
								RedirectTo = @Url.Action("Index", "Usuarios")
							}, JsonRequestBehavior.AllowGet);

		}

		private UserSignedInViewModel BuildUserMenus(User result, UserSignedInViewModel userSignedIn)
		{
			foreach (var userGroup in result.Groups)
			{
				foreach (var menuItem in userGroup.Group.Menus)
				{
					if (userSignedIn.UserMenus.All(x => x.Id != menuItem.Menu.Id))
					{
						var menu = new UserMenuViewModel {Id = menuItem.Menu.Id, Description = menuItem.Menu.Description};

						foreach (var feature in menuItem.Menu.Features)
						{
							menu.Features.Add(new FeaturesViewModel
								                  {
									                  Id = feature.Id,
									                  Description = feature.Description,
									                  Sequence = feature.Sequence,
									                  Url = feature.Url,
									                  DisplayOnScreen = feature.DisplayOnScreen
								                  });
						}

						userSignedIn.UserMenus.Add(menu);
					}
				}
			}

			return userSignedIn;
		}
	}
}
