﻿using System;
using System.Collections.Generic;
using NeuroCloud.Application;
using NeuroCloud.Application.SearchServices;
using NeuroCloud.Domain.Entities;
using NeuroCloud.Domain.Enums;
using NeuroCloud.Domain.QueryObjects;
using NeuroCloud.UI.Site.ViewModels;
using NeuroCloud.UI.Site.ViewModels.Rooms;
using System.Linq;
using System.Web.Mvc;

namespace NeuroCloud.UI.Site.Controllers
{
    public class QuartosController : BaseController
    {
	    private ISearchRoomsService SearchRoomsService { get; set; }
	    private ISearchSectorsService SearchSectorsService { get; set; }
        private IRoomCrudService RoomCrudService { get; set; }

        public QuartosController(ISearchRoomsService searchRoomsService, ISearchSectorsService searchSectorsService, IRoomCrudService roomCrudService )
	    {
	        this.SearchRoomsService = searchRoomsService;
	        this.SearchSectorsService = searchSectorsService;
	        this.RoomCrudService = roomCrudService;
	    }
         
        [HttpGet, OutputCache(Duration = 1, NoStore = true)]
        public ActionResult Index()
		{
	        return RedirectToAction("ListarQuartos", "Quartos");
        }

        [HttpGet, OutputCache(Duration = 1, NoStore = true)]
		public ActionResult ListarQuartos()
		{
			var viewModel = new ListRoomsViewModel();
			var sectors = this.SearchSectorsService.GetSectors();

		    foreach (var sector in sectors)
		    {
		        viewModel.Sectors.Add(new KeyValueViewModel<long> {Id = sector.Id, Value = sector.Description});
		    }

			return View("ListRooms", viewModel);
		}

        [HttpGet, OutputCache(Duration = 1, NoStore = true)]
		public ActionResult Novo()
		{
            var sectors = this.SearchSectorsService.GetSectors();
            var viewModel = new CrudRoomViewModel();
            
            foreach (var sector in sectors)
            {
                viewModel.Sectors.Add(new KeyValueViewModel<long> { Id = sector.Id, Value = sector.Description });
            }

			return View("CrudRoom", viewModel);
		}

        [HttpPost, OutputCache(Duration = 1, NoStore = true)]
	    public JsonResult NovoQuarto(CrudRoomViewModel viewModel)
	    {
	        var sectors = this.SearchSectorsService.GetSectors();
			var room = new Room();
			room.Description = viewModel.Description;
			room.Status = Status.Active;
			room.Sector = sectors.FirstOrDefault(x => x.Id == viewModel.SectorId);
			
			if (viewModel.RoomType == 1)
			{
				// Private Room
				room.Type = RoomType.Private;
				room.Beds.Add(new Bed {Description = "Quarto 1", Number = 1, Situation = Situation.Free, Room = room});
			}
			else
			{
				// Collective Room
				var number = 0;

				foreach (var bed in viewModel.Beds)
				{
					room.Beds.Add(new Bed {Description = bed.Value, Number = ++number, Room = room, Situation = Situation.Free});
				}

				room.Type = RoomType.Collective;
			}

			try
			{
				this.RoomCrudService.SaveNewRoom(room);
				return Json(new { Status = "Success", Message = "Quarto salvo com sucesso." }, JsonRequestBehavior.DenyGet);
			}
			catch (Exception exception)
			{
				return this.ErrorResult(exception.Message, JsonRequestBehavior.DenyGet);
			}
	    }

        [HttpGet, OutputCache(Duration = 1, NoStore = true)]
		public JsonResult PesquisarQuartos(ListRoomsViewModel viewModel)
		{
			var queryObject = new SearchRoomQueryObject
				                  {
					                  Description = viewModel.Description,
					                  SectorId = viewModel.SectorId,
					                  CurrentPage = viewModel.Page
				                  };

			var rooms = this.SearchRoomsService.SearchRooms(queryObject);

            var viewModelResult = rooms.Select(room => new SearchRoomsResultViewModel
				                                           {
					                                           Id = room.Id,
					                                           Description = room.Description,
					                                           SectorDescription = room.Sector.Description,
					                                           RoomType = room.Type == RoomType.Private ? "Privativo" : "Coletivo"
				                                           }).ToList();

			ViewData["QueryObject"] = queryObject;

            return Json(new
            {
                SearchResult = RenderPartialView(this, "_SearchRoomsResult", viewModelResult),
                Pager = RenderPartialView(this, "_Pager", queryObject)
            }, JsonRequestBehavior.AllowGet);
		}

        [HttpGet, OutputCache(Duration = 1, NoStore = true)]
        public JsonResult ExcluirQuarto(Int64 id)
        {
            Room room = null;

            try
            {
                room = this.SearchRoomsService.GetById(id);
                this.RoomCrudService.InactivateUser(room);
            }
            catch (InvalidOperationException invalidOperationException)
            {
                return this.ErrorResult(invalidOperationException.Message, JsonRequestBehavior.DenyGet);
            }

            if (room != null && room.Status == Status.Inactive)
            {
                return Json(new { Status = "Success", Message = string.Format("Quarto '{0}' excluído com sucesso.", room.Description) }, JsonRequestBehavior.DenyGet);
            }

            return Json(new { Status = "Error", Message = string.Format("Ocorreu um erro ao excluir o quarto '{0}'.", room.Description) }, JsonRequestBehavior.DenyGet);
        }

        [HttpGet, OutputCache(Duration = 1, NoStore = true)]
		public ActionResult EditarQuarto(long id)
		{
			var viewModel = new CrudRoomViewModel();
			var sectors = this.SearchSectorsService.GetSectors();
			var room = this.SearchRoomsService.GetById(id);

			viewModel.Description = room.Description;
			viewModel.Id = room.Id;
			viewModel.RoomType = (int) room.Type;
			viewModel.SectorId = room.Sector.Id;

            foreach (var sector in sectors)
            {
                viewModel.Sectors.Add(new KeyValueViewModel<long>
                {
                    Value = sector.Description,
                    Id = sector.Id
                });
            }

            foreach (var bed in room.Beds)
            {
                viewModel.Beds.Add(new KeyValueViewModel<long> {Id = bed.Id, Value = bed.Description});
            }

			return View("CrudRoom", viewModel);
		}

        [HttpPost, OutputCache(Duration = 1, NoStore = true)]
	    public JsonResult SalvarQuarto(CrudRoomViewModel viewModel)
		{
			try
			{
                var room = new Room
                {
                    Id = viewModel.Id,
                    Sector = new Sector { Id = viewModel.SectorId },
                    Description = viewModel.Description,
                    Type = (RoomType)viewModel.RoomType
                };

                viewModel.Beds.ToList().ForEach(x => room.Beds.Add(new Bed { Id = x.Id, Description = x.Value }));

				this.RoomCrudService.UpdateRoom(room);

				return Json(new { Status = "Success", Message = "Quarto atualizado com sucesso." }, JsonRequestBehavior.DenyGet);
			}
			catch (Exception exception)
			{
				return this.ErrorResult(exception.Message, JsonRequestBehavior.DenyGet);
			}
		}
    }
}
