﻿using System.Collections.Generic;
using NHibernate;
using NeuroCloud.Domain.Entities;
using NeuroCloud.Domain.Enums;
using NeuroCloud.Domain.Repositories;

namespace NeuroCloud.Infrastructure.Repositories
{
	internal class MenuRepository : NHibernateRepositoryBase<Menu>, IMenuRepository
	{
		public MenuRepository(ISession session) : base(session)
		{
		}

		public IList<Menu> GetAll()
		{
			return this.Session.QueryOver<Menu>()
			           .Where(x => x.Status == Status.Active)
			           .List();
		}
	}
}