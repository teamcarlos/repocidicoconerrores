﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NHibernate;
using NHibernate.Criterion;
using NeuroCloud.Domain.Entities;
using NeuroCloud.Domain.QueryObjects;
using NeuroCloud.Domain.Repositories;

namespace NeuroCloud.Infrastructure.Repositories
{
	public class PatientRepository : NHibernateRepositoryBase<Patient>, IPatientRepository
	{
		public PatientRepository(ISession session) : base(session)
		{
		}

	    public ICollection<Patient> Search(SearchPatientQueryObject queryObject)
	    {
	        var query = this.Session.QueryOver<Patient>();

            if (queryObject.Id > 0)
            {
                query = query.Where(x => x.Id == queryObject.Id);
            }

	        if (!string.IsNullOrEmpty(queryObject.FirstName))
	        {
	            query = query.WhereRestrictionOn(x => x.FirstName).IsLike(queryObject.FirstName, MatchMode.Anywhere);
	        }

            if (!string.IsNullOrEmpty(queryObject.LastName))
            {
                query = query.WhereRestrictionOn(x => x.LastName).IsLike(queryObject.LastName, MatchMode.Anywhere);
            }

            if (!string.IsNullOrEmpty(queryObject.CPF))
            {
                query = query.WhereRestrictionOn(x => x.CPF).IsLike(queryObject.CPF, MatchMode.Anywhere);
            }

			var recordCount = query.ToRowCountInt64Query().FutureValue<long>();

			var result = query.Skip(queryObject.RecordPerPage * (queryObject.CurrentPage - 1))
							  .Take(queryObject.RecordPerPage)
							  .Future();

			queryObject.RecordCount = recordCount.Value;

	        return result.ToList();
	    }
	}
}
