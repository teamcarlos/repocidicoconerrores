﻿using System.Linq;
using NHibernate;
using NeuroCloud.Domain.Entities;
using NeuroCloud.Domain.Repositories;

namespace NeuroCloud.Infrastructure.Repositories
{
    public class NHibernateRepositoryBase<T> : IRepositoryBase<T> where T : EntityBase
	{
		private readonly ISession session;

		internal ISession Session
		{
			get { return this.session; }
		}

		public NHibernateRepositoryBase(ISession session)
		{
			this.session = session;
		}

		public T Save(T obj)
		{
			this.Session.SaveOrUpdate(obj);

			return obj;
		}

		public void Delete(T obj)
		{
			this.Session.Delete(obj);
		}

		public T GetById(long id)
		{
			var query = this.Session.QueryOver<T>()
			                .Where(x => x.Id == id);

			return query.SingleOrDefault();
		}

		public T[] GetByIds(long[] ids)
		{
			var query = this.Session.QueryOver<T>()
			                .WhereRestrictionOn(x => x.Id).IsIn(ids);

			return query.List().ToArray();
		}
	}
}