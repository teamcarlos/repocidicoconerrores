﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NeuroCloud.Domain.Enums;
using NeuroCloud.Domain.QueryObjects;
using NHibernate;
using NeuroCloud.Domain.Entities;
using NeuroCloud.Domain.Repositories;
using NHibernate.Criterion;

namespace NeuroCloud.Infrastructure.Repositories
{
    public class GroupRepository : NHibernateRepositoryBase<Group>, IGroupRepository
    {
        public GroupRepository(ISession session)
            : base(session)
        {
        }

        public ICollection<Group> GetAll()
        {
            var query = this.Session.QueryOver<Group>()
                           .List<Group>();

            return query.ToList();
        }

        public ICollection<Group> SearchGroups(SearchUserQueryObject queryObject)
        {
            var query = this.Session.QueryOver<Group>().Where(x => x.Status != Status.Inactive);

            if (!string.IsNullOrEmpty(queryObject.Name))
            {
                query.Where(Restrictions.On<Group>(x => x.Description).IsLike(queryObject.Name, MatchMode.Anywhere));
            }

            var recordCount = query.ToRowCountInt64Query().FutureValue<long>();

            var result = query.Skip(queryObject.RecordPerPage * (queryObject.CurrentPage - 1))
                              .Take(queryObject.RecordPerPage)
                              .Future();

            queryObject.RecordCount = recordCount.Value;

            return result.ToList();
        }
    }
}
