﻿using System.Collections.Generic;
using System.Linq;
using NeuroCloud.Domain.Enums;
using NHibernate;
using NHibernate.Criterion;
using NeuroCloud.Domain.Entities;
using NeuroCloud.Domain.QueryObjects;
using NeuroCloud.Domain.Repositories;

namespace NeuroCloud.Infrastructure.Repositories
{
    public class RoomRepository : NHibernateRepositoryBase<Room>, IRoomRepository
    {
        public RoomRepository(ISession session) : base(session)
        {
        }

	    public IList<Room> SearchRooms(SearchRoomQueryObject queryObject)
	    {
		    var query = this.Session.QueryOver<Room>().Where(x=> x.Status == Status.Active);

			if (queryObject.Id > 0)
			{
				query = query.Where(x => x.Id == queryObject.Id);
			}

			if (!string.IsNullOrEmpty(queryObject.Description))
			{
				query = query.WhereRestrictionOn(x => x.Description).IsLike(queryObject.Description, MatchMode.Anywhere);
			}

			if (queryObject.SectorId > 0)
			{
				query = query.Where(x => x.Sector.Id == queryObject.SectorId);
			}

			var recordCount = query.ToRowCountInt64Query().FutureValue<long>();

			var result = query.Skip(queryObject.RecordPerPage * (queryObject.CurrentPage - 1))
							  .Take(queryObject.RecordPerPage)
							  .Future();

			queryObject.RecordCount = recordCount.Value;

		    return result.ToList();
	    }
    }
}