﻿//#define DBHOME
#define DBAZURE

using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using Iesi.Collections.Generic;
using NHibernate;
using NHibernate.Cfg;
using NHibernate.Cfg.MappingSchema;
using NHibernate.Dialect;
using NHibernate.Mapping;
using NHibernate.Mapping.ByCode;
using NHibernate.Tool.hbm2ddl;
using NeuroCloud.Domain.Entities;
using NeuroCloud.Infrastructure.Repositories.Mappings;
using Configuration = NHibernate.Cfg.Configuration;

namespace NeuroCloud.Infrastructure.Repositories.Config
{
	public class NHInitializer
	{
		public static ISessionFactory SessionFactory { get; set; }
		public static Configuration Configuration { get; set; }

		public static void Init()
		{
			var mapper = new ModelMapper();

			Configuration = new Configuration();

			Configuration.DataBaseIntegration(c =>
												  {

#if DBAZURE
													  c.Dialect<MsSqlAzureDialect>();
													  c.ConnectionString = @"Data Source=dtnsnlaows.database.windows.net;Database=DBNeuroCloud;User Id=userneurocloud;Password=@nEuro2013;";
#else
													  c.Dialect<MsSql2012Dialect>();
													  c.ConnectionString = @"Data Source=KEPLERVM-SQLSERVER;Database=NeuroCloud;User Id=NeuroCloud;Password=NeuroCloud.123;";
#endif
													  c.KeywordsAutoImport = Hbm2DDLKeyWords.AutoQuote;
													  c.LogSqlInConsole = true;
													  c.LogFormattedSql = true;
													  c.BatchSize = 100;
													  //c.SchemaAction = SchemaAutoAction.Create;
													  //c.AutoCommentSql = true;
												  });


			mapper.BeforeMapClass += (mi, t, map) => map.Id(prop => prop.Column(t.Name + "Id"));

			var mapping = MapEntities(mapper);
			var mappingxml = mapping.AsString();

			var entities = (from t in typeof(User).Assembly.GetExportedTypes()
							where t.IsSubclassOf(typeof(EntityBase)) || t.Namespace.Contains("Domain.Entities")
							select t).ToArray();
			
			Configuration.AddMapping(mapping);
			Configuration.AddAuxiliaryDatabaseObject(CreateHighLowScript(mapper.ModelInspector, entities));
			
			var exportSchema = !string.IsNullOrEmpty(ConfigurationManager.AppSettings["ExportSchema"]) && Convert.ToBoolean(ConfigurationManager.AppSettings["ExportSchema"]);

#if DBHOME
			
			if (exportSchema)
			{
				var se = new SchemaExport(Configuration);
				se.SetOutputFile(@"C:\\temp\\sql.txt");
				se.Drop(true, true);
				se.Create(true, true);
			}
#else
			//if (exportSchema)
			//{
			//	var se = new SchemaExport(Configuration);
			//	se.SetOutputFile(@"C:\\temp\\sql.txt");
			//	se.Drop(true, false);
			//	se.Create(true, false);
			//}
#endif
			
			SessionFactory = Configuration.BuildSessionFactory();
		}

		private static HbmMapping MapEntities(ModelMapper mapper)
		{
			var mappings = typeof(UserMapping)
				.Assembly
				.GetTypes()
				.Where(x => x.Name.EndsWith("Mapping"));

			mapper.AddMappings(mappings);

			var domainMapping = mapper.CompileMappingForAllExplicitlyAddedEntities();

			return domainMapping;
		}


		private static IAuxiliaryDatabaseObject CreateHighLowScript(IModelInspector inspector, IEnumerable<Type> entities)
		{
			var script = new StringBuilder();

			script.AppendLine("DELETE FROM NextHighValues;");
			script.AppendLine("ALTER TABLE NextHighValues ADD EntityName VARCHAR(128) NOT NULL;");

#if DBHOME
			script.AppendLine("CREATE NONCLUSTERED INDEX IdxNextHighValuesEntity ON NextHighValues (EntityName ASC);");
			script.AppendLine("GO");

			foreach (var entity in entities.Where(x => inspector.IsRootEntity(x)))
			{
				script.AppendLine(string.Format("INSERT INTO NextHighValues (EntityName, NextHigh) VALUES ('{0}',1);", entity.Name));
			}


			return new SimpleAuxiliaryDatabaseObject(script.ToString(), null,
			                                         new HashedSet<string>
				                                         {
					                                         typeof (MsSql2012Dialect).FullName,
					                                         typeof (MsSql2012Dialect).FullName
				                                         });
#else
			script.AppendLine("CREATE CLUSTERED INDEX IdxNextHighValuesEntity ON NextHighValues (EntityName ASC);");
			script.AppendLine("GO");

			foreach (var entity in entities.Where(x => inspector.IsRootEntity(x)))
			{
				script.AppendLine(string.Format("INSERT INTO NextHighValues (EntityName, NextHigh) VALUES ('{0}',1);", entity.Name));
			}

			return new SimpleAuxiliaryDatabaseObject(script.ToString(), null, new HashedSet<string> { typeof(MsSqlAzureDialect).FullName, typeof(MsSqlAzureDialect).FullName });
#endif
		}

#if DBAZURE
		public class MsSqlAzureDialect : MsSql2012Dialect
		{
			public override string PrimaryKeyString
			{
				get { return "primary key CLUSTERED"; }
			}
		}
#endif
	}
}