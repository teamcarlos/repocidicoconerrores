﻿using System;
using System.Data;
using Autofac;
using NHibernate;

namespace NeuroCloud.Infrastructure.Repositories.Config
{
    public class PersistenceModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
	        builder.Register(c => NHInitializer.SessionFactory.OpenSession())
	               .As<ISession>()
	               .OnActivated(args => args.Instance.BeginTransaction(IsolationLevel.ReadCommitted))
	               .OnRelease(session =>
		                          {
			                          try
			                          {
				                          session.Transaction.Commit();
			                          }
			                          catch (Exception exception)
			                          {
				                          session.Transaction.Rollback();
				                          throw;
			                          }
		                          }).InstancePerLifetimeScope();

            builder.RegisterAssemblyTypes(typeof (UserRepository).Assembly).AsImplementedInterfaces();
        }
    }
}
