﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NHibernate;
using NeuroCloud.Domain.Entities;
using NeuroCloud.Domain.Repositories;

namespace NeuroCloud.Infrastructure.Repositories
{
	internal class SectorRepository : NHibernateRepositoryBase<Sector>, ISectorRepository
	{
		public SectorRepository(ISession session) : base(session)
		{
		}

		public IList<Sector> GetAllSectors()
		{
			var query = this.Session.QueryOver<Sector>()
			                .List();

			return query;
		}
	}
}
