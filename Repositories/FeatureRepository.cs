﻿using NHibernate;
using NeuroCloud.Domain.Entities;
using NeuroCloud.Domain.Repositories;

namespace NeuroCloud.Infrastructure.Repositories
{
	internal class FeatureRepository : NHibernateRepositoryBase<Feature>, IFeatureRepository
	{
		public FeatureRepository(ISession session) : base(session)
		{
		}
	}
}
