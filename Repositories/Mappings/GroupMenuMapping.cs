﻿using NHibernate.Mapping.ByCode;
using NeuroCloud.Domain.Entities;

namespace NeuroCloud.Infrastructure.Repositories.Mappings
{
    internal class GroupMenuMapping : EntityMapping<GroupMenu>
    {
		public GroupMenuMapping()
        {
            Table("GroupMenus");

            ManyToOne(x => x.Group, mapper =>
                {
                    mapper.Fetch(FetchKind.Join);
                    mapper.Lazy(LazyRelation.NoLazy);
                    mapper.NotNullable(true);
                    mapper.Cascade(Cascade.None);
                    mapper.Column("GroupId");
                });

            ManyToOne(x => x.Menu, mapper =>
                {
                    mapper.Column("MenuId");
                    mapper.Fetch(FetchKind.Join);
                    mapper.Lazy(LazyRelation.NoLazy);
                    mapper.NotNullable(true);
                    mapper.Cascade(Cascade.None);
                });

            Property(x=> x.Status, mapper => mapper.NotNullable(true));
        }
    }
}