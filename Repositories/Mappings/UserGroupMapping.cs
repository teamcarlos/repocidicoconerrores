﻿using NHibernate.Mapping.ByCode;
using NeuroCloud.Domain.Entities;

namespace NeuroCloud.Infrastructure.Repositories.Mappings
{
	internal class UserGroupMapping:EntityMapping<UserGroup>
	{
		public UserGroupMapping()
		{
			Table("UserGroups");

			ManyToOne(x => x.User, mapper =>
				                       {
					                       mapper.Fetch(FetchKind.Join);
					                       mapper.Lazy(LazyRelation.NoLazy);
					                       mapper.NotNullable(true);
					                       mapper.Cascade(Cascade.None);
					                       mapper.Column("UserId");
				                       });

			ManyToOne(x => x.Group, mapper =>
				                        {
					                        mapper.Column("GroupId");
					                        mapper.Fetch(FetchKind.Join);
					                        mapper.Lazy(LazyRelation.NoLazy);
					                        mapper.NotNullable(true);
					                        mapper.Cascade(Cascade.None);
				                        });

			Property(x => x.Status, mapper => mapper.NotNullable(true));
		}
	}
}