﻿using NHibernate.Mapping.ByCode;
using NeuroCloud.Domain.Entities;

namespace NeuroCloud.Infrastructure.Repositories.Mappings
{
	internal class MenuMapping : EntityMapping<Menu>
	{
		public MenuMapping()
		{
			Table("Menus");

			Property(x => x.Description, mapper => { mapper.Length(50); mapper.NotNullable(true); });
			Property(x => x.Sequence, mapper => mapper.NotNullable(true));
			Property(x => x.BlockedAt, mapper => mapper.NotNullable(false));
			Property(x => x.Status, mapper => mapper.NotNullable(true));

			Set(x => x.Groups, mapper =>
									 {
										 mapper.Cascade(Cascade.None);
										 mapper.Fetch(CollectionFetchMode.Select);
										 mapper.Inverse(true);
										 mapper.Lazy(CollectionLazy.Extra);
										 mapper.Key(keyMapper =>
														{
															keyMapper.Column("MenuId");
															keyMapper.ForeignKey("MenuId_GroupMenu");
															keyMapper.NotNullable(true);
														});
									 }, relation => relation.OneToMany());

			Set(x => x.Features, mapper =>
				                     {
					                     mapper.Cascade(Cascade.All | Cascade.DeleteOrphans);
					                     mapper.Fetch(CollectionFetchMode.Select);
					                     mapper.Inverse(true);
					                     mapper.Lazy(CollectionLazy.Extra);
					                     mapper.Key(keyMapper =>
						                                {
							                                keyMapper.Column("MenuId");
							                                keyMapper.ForeignKey("MenuId_Feature");
							                                keyMapper.NotNullable(true);
						                                });
				                     }, relation => relation.OneToMany());
		}
	}
}