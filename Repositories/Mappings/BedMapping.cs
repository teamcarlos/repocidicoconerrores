﻿using NHibernate.Mapping.ByCode;
using NeuroCloud.Domain.Components;
using NeuroCloud.Domain.Entities;

namespace NeuroCloud.Infrastructure.Repositories.Mappings
{
	internal class BedMapping : EntityMapping<Bed>
	{
		public BedMapping()
		{
			Table("Beds");

			Property(x => x.Description, mapper => { mapper.NotNullable(true); mapper.Length(50); });
			Property(x => x.Number, mapper => mapper.NotNullable(true));
			Property(x => x.Situation, mapper => mapper.NotNullable(true));

			ManyToOne(x => x.Room, mapper =>
									   {
										   mapper.Cascade(Cascade.None);
										   mapper.Fetch(FetchKind.Join);
										   mapper.Lazy(LazyRelation.Proxy);
										   mapper.ForeignKey("RoomId_Bed");
									   });
		}
	}
}