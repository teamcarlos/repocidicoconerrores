﻿using NeuroCloud.Domain.Entities;

namespace NeuroCloud.Infrastructure.Repositories.Mappings
{
	internal class PatientMapping : EntityMapping<Patient>
	{
		public PatientMapping()
		{
			Table("Patients");

			Property(x => x.FirstName, map =>
				                           {
					                           map.NotNullable(true);
					                           map.Length(50);
				                           });

			Property(x => x.LastName, map =>
				                          {
					                          map.NotNullable(true);
					                          map.Length(50);
				                          });

			Property(x => x.CPF, map => map.Length(15));
			Property(x => x.RG, map => map.Length(15));
			Property(x => x.BirthDate);
			Property(x => x.Email, map => map.Length(50));
			Property(x => x.CreatedAt, map => map.NotNullable(true));
			Property(x => x.Sex, mapper => mapper.NotNullable(true));
			Property(x => x.CellPhone, mapper =>
				                           {
					                           mapper.NotNullable(false);
					                           mapper.Length(15);
				                           });

			Property(x => x.HomePhone, mapper =>
				                           {
					                           mapper.NotNullable(false);
					                           mapper.Length(15);
				                           });

			Component(x => x.Address, mapper =>
				                          {
					                          mapper.Property(x => x.Street, propertyMapper =>
						                                                         {
							                                                         propertyMapper.NotNullable(false);
							                                                         propertyMapper.Length(100);
						                                                         });

					                          mapper.Property(x => x.Number, propertyMapper =>
						                                                         {
							                                                         propertyMapper.NotNullable(false);
							                                                         propertyMapper.Length(5);
						                                                         });

					                          mapper.Property(x => x.Complement, propertyMapper =>
						                                                             {
							                                                             propertyMapper.NotNullable(false);
							                                                             propertyMapper.Length(100);
						                                                             });

					                          mapper.Property(x => x.District, propertyMapper =>
						                                                           {
							                                                           propertyMapper.NotNullable(false);
							                                                           propertyMapper.Length(50);
						                                                           });

					                          mapper.Property(x => x.City, propertyMapper =>
						                                                       {
							                                                       propertyMapper.NotNullable(false);
							                                                       propertyMapper.Length(100);
						                                                       });

					                          mapper.Property(x => x.State, propertyMapper =>
						                                                        {
							                                                        propertyMapper.NotNullable(false);
							                                                        propertyMapper.Length(50);
						                                                        });

					                          mapper.Property(x => x.ZipCode, propertyMapper =>
						                                                          {
							                                                          propertyMapper.NotNullable(false);
							                                                          propertyMapper.Length(10);
						                                                          });
				                          });
		}
	}
}