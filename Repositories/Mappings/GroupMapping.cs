﻿using NHibernate.Mapping.ByCode;
using NeuroCloud.Domain.Entities;

namespace NeuroCloud.Infrastructure.Repositories.Mappings
{
    internal class GroupMapping : EntityMapping<Group>
    {
        public GroupMapping()
        {
            Table("Groups");

            Property(x => x.Description, mapper => { mapper.Length(50); mapper.NotNullable(true); });
            Property(x => x.Status, mapper => mapper.NotNullable(true));

            Set(x => x.Menus, mapper =>
                {
                    mapper.Lazy(CollectionLazy.Extra);
                    mapper.Inverse(true);
                    mapper.Fetch(CollectionFetchMode.Select);
                    mapper.Cascade(Cascade.All | Cascade.DeleteOrphans);
                    mapper.Key(keyMapper =>
                        {
                            keyMapper.Column("GroupId");
                            keyMapper.ForeignKey("GroupId_GroupMenu");
                            keyMapper.NotNullable(true);
                        });
                }, relation => relation.OneToMany());
        }
    }
}