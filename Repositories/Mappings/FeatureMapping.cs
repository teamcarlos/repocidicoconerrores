﻿using NHibernate.Mapping.ByCode;
using NeuroCloud.Domain.Entities;

namespace NeuroCloud.Infrastructure.Repositories.Mappings
{
	internal class FeatureMapping : EntityMapping<Feature>
	{
		public FeatureMapping()
		{
			Table("Features");

			Property(x => x.Description, mapper => { mapper.Length(50); mapper.NotNullable(true); });
			Property(x => x.Sequence, mapper => mapper.NotNullable(true));
			Property(x => x.Url, mapper => { mapper.NotNullable(true); mapper.Length(255); });
			Property(x=> x.Status, mapper => mapper.NotNullable(true));
			Property(x=> x.DisplayOnScreen, mapper => mapper.NotNullable(true));
			
			ManyToOne(x=> x.Menu, mapper =>
				                      {
					                      mapper.Cascade(Cascade.None);
										  mapper.Fetch(FetchKind.Join);
										  mapper.Lazy(LazyRelation.NoLazy);
										  mapper.Column("MenuId");
										  mapper.NotNullable(true
											  );
				                      });
		}
	}
}