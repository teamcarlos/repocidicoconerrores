﻿using NHibernate.Mapping.ByCode;
using NeuroCloud.Domain.Entities;

namespace NeuroCloud.Infrastructure.Repositories.Mappings
{
	internal class UserMapping : EntityMapping<User>
	{
		public UserMapping()
		{
			Table("Users");

			Property(x => x.FirstName, map => { map.NotNullable(true); map.Length(50); });
			Property(x => x.LastName, map => { map.NotNullable(true); map.Length(50); });
			Property(x => x.CPF, map => map.Length(15));
			Property(x => x.RG, map => map.Length(15));
			Property(x => x.BirthDate);
			Property(x => x.Email, map => map.Length(50));
			Property(x => x.Record, map => map.Length(10));
			Property(x => x.CreatedAt, map => map.NotNullable(true));
			Property(x => x.Username, map => { map.NotNullable(true); map.Length(20); });
			Property(x => x.Password, map => { map.NotNullable(true); map.Length(30); });
			Property(x => x.Status, mapper => mapper.NotNullable(true));
			Property(x => x.BlockedAt, mapper => mapper.NotNullable(false));

			ManyToOne(x => x.Sector, mapper =>
										 {
											 mapper.Cascade(Cascade.None);
											 mapper.Fetch(FetchKind.Join);
											 mapper.Column("SectorId");
											 mapper.ForeignKey("SectorId_User");
											 mapper.Lazy(LazyRelation.Proxy);
											 mapper.NotNullable(false);
										 });

			Set(x => x.Groups, mapper =>
			{
				mapper.Lazy(CollectionLazy.Extra);
				mapper.Inverse(true);
				mapper.Fetch(CollectionFetchMode.Select);
				mapper.Cascade(Cascade.All | Cascade.DeleteOrphans);
				mapper.Key(keyMapper =>
				{
					keyMapper.Column("UserId");
					keyMapper.ForeignKey("UserId_UserGroup");
					keyMapper.NotNullable(true);
				});
			}, relation => relation.OneToMany());
		}
	}
}

