﻿using NeuroCloud.Domain.Entities;

namespace NeuroCloud.Infrastructure.Repositories.Mappings
{
	internal class SectorMapping : EntityMapping<Sector>
	{
		public SectorMapping()
		{
            Table("Sectors");

			Property(prop => prop.Description, mapper => { mapper.Length(100); mapper.NotNullable(true); });
		}
	}
}