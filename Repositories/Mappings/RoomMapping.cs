﻿using NHibernate.Mapping.ByCode;
using NeuroCloud.Domain.Entities;

namespace NeuroCloud.Infrastructure.Repositories.Mappings
{
	internal class RoomMapping : EntityMapping<Room>
	{
		public RoomMapping()
		{
			Table("Rooms");

			Property(x => x.Description, mapper => { mapper.Length(50); mapper.NotNullable(true); });
			Property(x => x.Status, mapper => mapper.NotNullable(true));
			Property(x => x.Type, mapper => mapper.NotNullable(true));

			ManyToOne(x => x.Sector, mapper =>
										 {
											 mapper.Cascade(Cascade.None);
											 mapper.Fetch(FetchKind.Join);
											 mapper.ForeignKey("SectorId_Room");
											 mapper.Lazy(LazyRelation.Proxy);
											 mapper.NotNullable(true);
										 });

			Set(x => x.Beds, mapper =>
				                 {
					                 mapper.Cascade(Cascade.All | Cascade.DeleteOrphans);
					                 mapper.Fetch(CollectionFetchMode.Select);
					                 mapper.Inverse(true);
				                 }, relation => relation.OneToMany(mapper => mapper.Class(typeof (Bed))));
		}
	}
}