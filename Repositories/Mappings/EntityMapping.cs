﻿using NHibernate.Mapping.ByCode;
using NHibernate.Mapping.ByCode.Conformist;
using NeuroCloud.Domain.Entities;

namespace NeuroCloud.Infrastructure.Repositories.Mappings
{
	internal class EntityMapping<T> : ClassMapping<T> where T : EntityBase
	{
		protected const string NH_HILO_TABLE = "NextHighValues";
		protected const string NH_HILO_COLUMN = "NextHigh";

		public EntityMapping()
		{
			Id(x => x.Id, mapper => mapper.Generator(Generators.HighLow, gmap => gmap.Params(new
				                                                                                 {
					                                                                                 max_low = 100,
					                                                                                 table = NH_HILO_TABLE,
					                                                                                 column = NH_HILO_COLUMN,
					                                                                                 where = string.Format("EntityName = '{0}'", typeof(T).Name)
				                                                                                 })));
		}
	}
}