﻿using System;
using System.Collections.Generic;
using System.Linq;
using NeuroCloud.Domain.Enums;
using NHibernate;
using NHibernate.Criterion;
using NHibernate.Linq;
using NeuroCloud.Domain.QueryObjects;
using NeuroCloud.Domain.Repositories;
using NeuroCloud.Domain.Entities;

namespace NeuroCloud.Infrastructure.Repositories
{
    internal class UserRepository : NHibernateRepositoryBase<User>, IUserRepository
    {
	    public UserRepository(ISession session) : base(session)
	    {
	    }

        public bool EmailExists(string email)
        {
            var query = this.Session.QueryOver<User>()
                            .Where(x => x.Email == email)
                            .RowCount();

            return query > 0;
        }

	    public bool LoginExists(string login)
	    {
			var query = this.Session.QueryOver<User>()
							.Where(x => x.Username == login)
							.RowCount();

			return query > 0;
	    }

        public ICollection<User> SearchUsers(SearchUserQueryObject queryObject)
        {
            var query = this.Session.QueryOver<User>();

            query.Where(x => x.Status == Status.Active);

            if (!string.IsNullOrEmpty(queryObject.Name))
            {
                query.Where(Restrictions.On<User>(x => x.FirstName).IsLike(queryObject.Name, MatchMode.Anywhere) ||
                            Restrictions.On<User>(x => x.LastName).IsLike(queryObject.Name, MatchMode.Anywhere));
            }

            if (queryObject.SectorId > 0)
            {
                query.Where(x => x.Sector.Id == queryObject.SectorId);
            }

            var recordCount = query.ToRowCountInt64Query().FutureValue<long>();

            var result = query.Skip(queryObject.RecordPerPage*(queryObject.CurrentPage - 1))
                              .Take(queryObject.RecordPerPage)
                              .Future();

            queryObject.RecordCount = recordCount.Value;

            return result.ToList();
        }

	    public User Login(string username, string password)
	    {
		    var query = this.Session.QueryOver<User>()
		                    .Where(x => x.Username == username && x.Password == password)
							.SingleOrDefault();
		    return query;
	    }
    }
}
