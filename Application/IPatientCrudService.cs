﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NeuroCloud.Domain.Entities;

namespace NeuroCloud.Application
{
	public interface IPatientCrudService
	{
		Patient SaveNewPatient(Patient patient);

		Patient UpdatePatient(Patient patient);
	    
        void InactivatePatient(Patient patient);
	}
}
