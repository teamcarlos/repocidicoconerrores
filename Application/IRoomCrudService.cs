﻿using System.Collections.Generic;
using NeuroCloud.Domain.Entities;

namespace NeuroCloud.Application
{
    public interface  IRoomCrudService
    {
        Room SaveNewRoom(Room room);

	    void UpdateRoom(Room updatedRoom);
        
        void InactivateUser(Room room);
    }
}