﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NeuroCloud.Domain.Entities;
using NeuroCloud.Domain.Enums;
using NeuroCloud.Domain.Repositories;

namespace NeuroCloud.Application.Implementations
{
	public class PatientCrudService : ApplicationBase, IPatientCrudService
	{
		private IPatientRepository PatientRepository { get; set; }

		public PatientCrudService(IPatientRepository patientRepository)
		{
			this.PatientRepository = patientRepository;
		}

		public Patient SaveNewPatient(Patient patient)
		{
			return this.PatientRepository.Save(patient);
		}

		public Patient UpdatePatient(Patient patient)
		{
			var currentPatient = this.PatientRepository.GetById(patient.Id);

			currentPatient.FirstName = patient.FirstName;
			currentPatient.LastName = patient.LastName;
			currentPatient.CPF = patient.CPF;
			currentPatient.RG = patient.RG;
			currentPatient.BirthDate = patient.BirthDate;
			currentPatient.Email = patient.Email;
			currentPatient.Sex = patient.Sex;

			currentPatient.Address.City = patient.Address.City;
			currentPatient.Address.Number = patient.Address.Number;
			currentPatient.Address.Complement = patient.Address.Complement;
			currentPatient.Address.District = patient.Address.District;
			currentPatient.Address.City = patient.Address.City;
			currentPatient.Address.State = patient.Address.State;
			currentPatient.Address.ZipCode = patient.Address.ZipCode;

			currentPatient.CellPhone = patient.CellPhone;
			currentPatient.HomePhone = patient.HomePhone;

			return this.PatientRepository.Save(currentPatient);
		}

	    public void InactivatePatient(Patient patient)
	    {
	        patient.Status = Status.Inactive;
	        this.PatientRepository.Save(patient);
	    }
	}
}
