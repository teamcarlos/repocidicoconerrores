﻿using System.Linq;
using NeuroCloud.Domain.Entities;
using NeuroCloud.Domain.Enums;
using NeuroCloud.Domain.Repositories;

namespace NeuroCloud.Application.Implementations
{
	public class RoomCrudService : ApplicationBase, IRoomCrudService
	{
		private IRoomRepository RoomRepository { get; set; }
		private ISectorRepository SectorRepository { get; set; }

		public RoomCrudService(IRoomRepository roomRepository, ISectorRepository sectorRepository)
		{
			this.RoomRepository = roomRepository;
			this.SectorRepository = sectorRepository;
		}

		public Room SaveNewRoom(Room room)
		{
			return this.RoomRepository.Save(room);
		}

		public void UpdateRoom(Room updatedRoom)
		{
			var bedNumber = 0;

			var currentRoom = this.RoomRepository.GetById(updatedRoom.Id);
			currentRoom.Description = updatedRoom.Description;
			currentRoom.Sector = this.SectorRepository.GetById(updatedRoom.Sector.Id);

			if (currentRoom.Type != updatedRoom.Type || updatedRoom.Type == RoomType.Private)
				currentRoom.Beds.Clear();

			if (updatedRoom.Type == RoomType.Private)
			{
				currentRoom.Type = RoomType.Private;

				var firstOrDefault = updatedRoom.Beds.FirstOrDefault();

				if (firstOrDefault == null)
				{
					currentRoom.Beds.Add(new Bed
											 {
												 Description = "Leito 1",
												 Number = 1,
												 Situation = Situation.Free,
												 Room = currentRoom
											 });

				}
			}
			else
			{
				currentRoom.Type = RoomType.Collective;

				var bedIdsOnDatabse = currentRoom.Beds.Select(x => x.Id).ToList();
				var bedsFromScreen = updatedRoom.Beds.Select(x => x.Id).Where(x => x != 0).ToList();

				foreach (var bedId in bedIdsOnDatabse)
				{
					if (!bedsFromScreen.Contains(bedId))
					{
						var item = currentRoom.Beds.FirstOrDefault(x => x.Id == bedId);
						currentRoom.Beds.Remove(item);
					}
				}

				foreach (var newBed in updatedRoom.Beds.Where(x => x.Id == 0).ToList())
				{
					currentRoom.Beds.Add(new Bed
											 {
												 Description = newBed.Description,
												 Number = ++bedNumber,
												 Room = currentRoom,
												 Situation = Situation.Free
											 });
				}

				foreach (var updatedBed in updatedRoom.Beds.Where(x => x.Id != 0).ToList())
				{
					var currentBed = currentRoom.Beds.FirstOrDefault(x => x.Id == updatedBed.Id);

					if (currentBed != null)
						currentBed.Description = updatedBed.Description;
				}
			}

			this.RoomRepository.Save(currentRoom);
		}

	    public void InactivateUser(Room room)
	    {
	        room.Status = Status.Inactive;
	        this.RoomRepository.Save(room);
	    }
	}
}
