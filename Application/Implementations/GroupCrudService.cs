﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NeuroCloud.Domain.Entities;
using NeuroCloud.Domain.Enums;
using NeuroCloud.Domain.Repositories;

namespace NeuroCloud.Application.Implementations
{
    public class GroupCrudService : ApplicationBase, IGroupCrudService
    {
        private IGroupRepository GroupRepository { get; set; }
        private IMenuRepository MenuRepository { get; set; }

        public GroupCrudService(IGroupRepository groupRepository, IMenuRepository menuRepository)
        {
            this.GroupRepository = groupRepository;
            MenuRepository = menuRepository;
        }

        public Group SaveNewGroup(Group entity)
        {
            var menus = this.MenuRepository.GetByIds(entity.Menus.Select(x => x.Menu.Id).ToArray());
            var groupMenus = new List<GroupMenu>();

            foreach (var menu in menus)
            {
                var firstOrDefault = entity.Menus.FirstOrDefault(x => x.Menu.Id == menu.Id);

                if (firstOrDefault != null)
                    groupMenus.Add(new GroupMenu { Menu = menu, Status = firstOrDefault.Status, Group = entity });
            }

            entity.Menus = groupMenus;

            return this.GroupRepository.Save(entity);
        }

        public Group UpdateGroup(Group entity)
        {
            return this.GroupRepository.Save(entity);
        }

        public Group InactivateGroup(Group groupEntity)
        {
            groupEntity.Status = Status.Inactive;

            return this.UpdateGroup(groupEntity);
        }
    }
}
