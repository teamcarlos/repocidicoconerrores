﻿using System;
using NeuroCloud.Domain.Entities;
using NeuroCloud.Domain.Enums;
using NeuroCloud.Domain.Repositories;

namespace NeuroCloud.Application.Implementations
{
	public class UserSignUp : ApplicationBase, IUserSignUpService
    {
        private IUserRepository UserRepository { get; set; }

        public UserSignUp(IUserRepository userRepository )
        {
            this.UserRepository = userRepository;
        }

        public bool EmailExists(string email)
        {
            return this.UserRepository.EmailExists(email);
        }

        public bool LoginExists(string login)
        {
	        return this.UserRepository.LoginExists(login);
        }

        public bool IsValid()
        {
            throw new System.NotImplementedException();
        }

        public User SaveNewUser(User user)
        {
			if (this.EmailExists(user.Email))
				throw new InvalidOperationException("E-mail já cadastrado.");

			if(this.LoginExists(user.Username))
				throw new InvalidOperationException("Login já cadastrado.");

			return this.UserRepository.Save(user);
        }

	    public User UpdateUser(User user)
	    {
		    return this.UserRepository.Save(user);
	    }

	    public User InactivateUser(User user)
	    {
            user.Status = Status.Inactive;
	        
            return this.UserRepository.Save(user);
	    }
    }
}