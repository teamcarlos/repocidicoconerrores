﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NeuroCloud.Domain.Entities;
using NeuroCloud.Domain.Repositories;

namespace NeuroCloud.Application.Implementations
{
	public class UserSignInServiceService : ApplicationBase, IUserSignInService
	{
		private IUserRepository UserRepository { get; set; }

		public UserSignInServiceService(IUserRepository userRepository)
		{
			this.UserRepository = userRepository;
		}

		public User SignIn(string username, string password)
		{
			return this.UserRepository.Login(username, password);
		}

		public void RememberPassword(string username)
		{
			throw new NotImplementedException();
		}
	}
}
