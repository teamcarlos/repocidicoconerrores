﻿using System.Collections.Generic;
using NeuroCloud.Domain.Entities;

namespace NeuroCloud.Application.SearchServices
{
	public interface ISearchSectorsService
	{
		IList<Sector> GetSectors();
	}
}