﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NeuroCloud.Domain.Entities;

namespace NeuroCloud.Application.SearchServices
{
	public interface ISearchMenusService
	{
		IList<Menu> GetAll();
	}
}
