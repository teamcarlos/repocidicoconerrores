﻿using System.Collections.Generic;
using NeuroCloud.Domain.Entities;
using NeuroCloud.Domain.QueryObjects;

namespace NeuroCloud.Application.SearchServices
{
	public interface ISearchUsersService
	{
		User GetById(long id);

        ICollection<User> SearchUsers(SearchUserQueryObject queryObject);
    }
}