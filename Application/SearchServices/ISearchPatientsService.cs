﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NeuroCloud.Domain.Entities;
using NeuroCloud.Domain.QueryObjects;

namespace NeuroCloud.Application.SearchServices
{
	public interface ISearchPatientsService
	{
		Patient GetById(long id);

	    ICollection<Patient> SearchPatients(SearchPatientQueryObject queryObject);
	}
}
