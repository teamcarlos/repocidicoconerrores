﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NeuroCloud.Domain.Entities;
using NeuroCloud.Domain.QueryObjects;

namespace NeuroCloud.Application.SearchServices
{
	public interface ISearchRoomsService
	{
		IList<Room> SearchRooms(SearchRoomQueryObject queryObject);
		
		Room GetById(long id);
	}
}
