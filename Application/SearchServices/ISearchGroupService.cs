﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NeuroCloud.Domain.Entities;
using NeuroCloud.Domain.QueryObjects;

namespace NeuroCloud.Application.SearchServices
{
    public interface ISearchGroupsService
    {
        ICollection<Group> GetAll();

        Group GetById(long id);

        ICollection<Group> SearchGroups(SearchUserQueryObject queryObject);
    }
}
