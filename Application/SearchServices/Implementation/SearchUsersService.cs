﻿using System;
using System.Collections.Generic;
using NeuroCloud.Domain.Entities;
using NeuroCloud.Domain.QueryObjects;
using NeuroCloud.Domain.Repositories;

namespace NeuroCloud.Application.SearchServices.Implementation
{
    public class SearchUsersService : SearchServiceBase, ISearchUsersService
    {
        private IUserRepository UserRepository { get; set; }

        public SearchUsersService(IUserRepository userRepository)
        {
            this.UserRepository = userRepository;
        }

		public User GetById(Int64 id)
		{
			return this.UserRepository.GetById(id);
		}

        public ICollection<User> SearchUsers(SearchUserQueryObject queryObject)
        {
            return this.UserRepository.SearchUsers(queryObject);
        }
    }
}
