﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NeuroCloud.Domain.Entities;
using NeuroCloud.Domain.QueryObjects;
using NeuroCloud.Domain.Repositories;

namespace NeuroCloud.Application.SearchServices.Implementation
{
	public class SearchPatientsService : SearchServiceBase, ISearchPatientsService
	{
		private IPatientRepository PatientRepository { get; set; }

		public SearchPatientsService(IPatientRepository patientRepository)
		{
			this.PatientRepository = patientRepository;
		}

		public Patient GetById(long id)
		{
			return this.PatientRepository.GetById(id);
		}

	    public ICollection<Patient> SearchPatients(SearchPatientQueryObject queryObject)
	    {
	        return this.PatientRepository.Search(queryObject);
	    }
	}
}
