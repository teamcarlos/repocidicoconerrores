﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NeuroCloud.Domain.Entities;
using NeuroCloud.Domain.Repositories;

namespace NeuroCloud.Application.SearchServices.Implementation
{
	public class SearchSectorsService : SearchServiceBase, ISearchSectorsService
	{
		private ISectorRepository SectorRepository { get; set; }

		public SearchSectorsService(ISectorRepository sectorRepository)
		{
			this.SectorRepository = sectorRepository;
		}

		public IList<Sector> GetSectors()
		{
			return this.SectorRepository.GetAllSectors();
		}
	}
}
