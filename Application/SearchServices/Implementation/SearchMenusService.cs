﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NeuroCloud.Domain.Entities;
using NeuroCloud.Domain.Repositories;

namespace NeuroCloud.Application.SearchServices.Implementation
{
	public class SearchMenusService : SearchServiceBase, ISearchMenusService
	{
		private IMenuRepository MenuRepository { get; set; }

		public SearchMenusService(IMenuRepository menuRepository)
		{
			this.MenuRepository = menuRepository;
		}

		public IList<Menu> GetAll()
		{
			return this.MenuRepository.GetAll();
		}
	}
}
