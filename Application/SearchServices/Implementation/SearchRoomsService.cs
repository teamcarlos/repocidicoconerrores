﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NeuroCloud.Application.Implementations;
using NeuroCloud.Domain.Entities;
using NeuroCloud.Domain.QueryObjects;
using NeuroCloud.Domain.Repositories;

namespace NeuroCloud.Application.SearchServices.Implementation
{
	public class SearchRoomsService : ApplicationBase, ISearchRoomsService
	{
		private IRoomRepository RoomRepository { get; set; }

		public SearchRoomsService(IRoomRepository roomRepository)
		{
			this.RoomRepository = roomRepository;
		}

		public IList<Room> SearchRooms(SearchRoomQueryObject queryObject)
		{
			return this.RoomRepository.SearchRooms(queryObject);
		}

		public Room GetById(long id)
		{
			return this.RoomRepository.GetById(id);
		}
	}
}
