﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NeuroCloud.Domain.Entities;
using NeuroCloud.Domain.QueryObjects;
using NeuroCloud.Domain.Repositories;

namespace NeuroCloud.Application.SearchServices.Implementation
{
    public class SearchGroupsService : SearchServiceBase, ISearchGroupsService
    {
        private IGroupRepository GroupRepository { get; set; }

        public SearchGroupsService(IGroupRepository groupRepository)
        {
            this.GroupRepository = groupRepository;
        }

        public ICollection<Group> GetAll()
        {
            return this.GroupRepository.GetAll();
        }

        public Group GetById(long id)
        {
            return this.GroupRepository.GetById(id);
        }

        public ICollection<Group> SearchGroups(SearchUserQueryObject queryObject)
        {
            return this.GroupRepository.SearchGroups(queryObject);
        }
    }
}
