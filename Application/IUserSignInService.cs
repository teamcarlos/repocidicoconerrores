﻿using NeuroCloud.Domain.Entities;

namespace NeuroCloud.Application
{
	public interface IUserSignInService
	{
		User SignIn(string username, string password);

		void RememberPassword(string username);
	}
}