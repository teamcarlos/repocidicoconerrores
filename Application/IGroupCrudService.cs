﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NeuroCloud.Domain.Entities;

namespace NeuroCloud.Application
{
    public interface IGroupCrudService
    {
        Group SaveNewGroup(Group group);

        Group UpdateGroup(Group group);

        Group InactivateGroup(Group groupEntity);
    }
}
