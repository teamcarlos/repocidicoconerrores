﻿using NeuroCloud.Domain.Entities;

namespace NeuroCloud.Application
{
	public interface IUserSignUpService
	{
		bool EmailExists(string email);
        
		bool LoginExists(string login);

		bool IsValid();

		User SaveNewUser(User user);
	    
		User UpdateUser(User user);
	    
        User InactivateUser(User user);
	}
}