﻿using System;
using Autofac;
using NeuroCloud.Domain.Entities;
using NeuroCloud.Domain.Repositories;
using NeuroCloud.Infrastructure.Repositories.Config;

namespace DataGenerator
{
	public class Program
	{
		private static void Main(string[] args)
		{
			NHInitializer.Init();

			var container = BuildContainer();

			using (var lt = container.BeginLifetimeScope())
			{
				var menusRepository = lt.Resolve<IMenuRepository>();
				var userRepository = lt.Resolve<IUserRepository>();
				var sectorRepository = lt.Resolve<ISectorRepository>();
				var groupRepository = lt.Resolve<IGroupRepository>();

				var menuGenerator = new MenuGenerator(menusRepository);
				var groupGenerator = new GroupGenerator(groupRepository);
				var userGenerator = new UserGenerator(userRepository);
				var sectorGenerator = new SectorGenerator(sectorRepository);

				var sectors = sectorGenerator.GenerateSectors();
				var menus = menuGenerator.GenerateMenus();
				var groups = groupGenerator.GenerateGroups(menus);
				var users = userGenerator.GenerateUsers(sectors, groups);
				
			}
		}

		private static IContainer BuildContainer()
		{
			var builder = new ContainerBuilder();

			builder.RegisterAssemblyModules(typeof(NHInitializer).Assembly);
			builder.RegisterAssemblyTypes(typeof(User).Assembly).AsImplementedInterfaces();

			var container = builder.Build();

			return container;
		}
	}
}
