﻿using System.Collections.Generic;
using System.Linq;
using NeuroCloud.Domain.Entities;
using NeuroCloud.Domain.Enums;
using NeuroCloud.Domain.Repositories;

namespace DataGenerator
{
	public class MenuGenerator
	{
		private IMenuRepository MenuRepository { get; set; }

		public MenuGenerator(IMenuRepository menuRepository)
		{
			this.MenuRepository = menuRepository;
		}

		public IList<Menu> GenerateMenus()
		{
			var menus = this.CreateObjects();

			foreach (var menu in menus)
			{
				this.MenuRepository.Save(menu);
			}

			return menus;
		}

		private IList<Menu> CreateObjects()
		{
			var menuList = new List<Menu>();

			var menuInicial = new Menu { Description = "Inicial", Sequence = 1, Status = Status.Active };
			menuInicial.Features.Add(new Feature { Description = "Home", Sequence = 1, Url = "~/Home/Inicial", DisplayOnScreen = true, Menu = menuInicial });
			menuList.Add(menuInicial);

			var menuCadastros = new Menu { Description = "Cadastros", Sequence = 2, Status = Status.Active };
			menuCadastros.Features.Add(new Feature { Description = "Usuários", Sequence = 1, Url = "~/Usuarios/ListarUsuarios", DisplayOnScreen = true, Status = Status.Active, Menu = menuCadastros });
			menuCadastros.Features.Add(new Feature { Description = "Cadastro de Usuários", Sequence = 2, DisplayOnScreen = false, Url = "~/Usuarios/Novo", Status = Status.Active, Menu = menuCadastros });
			menuCadastros.Features.Add(new Feature { Description = "Editar de Usuário", Sequence = 3, Url = "~/Usuarios/EditarUsuario", DisplayOnScreen = false, Status = Status.Active, Menu = menuCadastros });

			menuCadastros.Features.Add(new Feature { Description = "Grupos", Sequence = 1, Url = "~/Grupos/", DisplayOnScreen = true, Menu = menuCadastros });
			menuCadastros.Features.Add(new Feature { Description = "Cadastro de Grupo", Sequence = 2, Url = "~/Grupos/Novo", DisplayOnScreen = false, Menu = menuCadastros });
			menuCadastros.Features.Add(new Feature { Description = "Editar de Grupo", Sequence = 3, Url = "~/Grupos/EditarGrupo", DisplayOnScreen = false, Menu = menuCadastros });

			menuCadastros.Features.Add(new Feature { Description = "Quartos", Sequence = 1, Url = "~/Quartos/ListarQuartos", DisplayOnScreen = true, Status = Status.Active, Menu = menuCadastros });
			menuCadastros.Features.Add(new Feature { Description = "Cadastro de Quarto", Sequence = 2, Url = "~/Quartos/Novo", DisplayOnScreen = false, Status = Status.Active, Menu = menuCadastros });
			menuCadastros.Features.Add(new Feature { Description = "Editar Quarto", Sequence = 3, Url = "~/Quartos/EditarQuartos", DisplayOnScreen = false, Status = Status.Active, Menu = menuCadastros });

			menuCadastros.Features.Add(new Feature { Description = "Pacientes", Sequence = 1, Url = "~/Pacientes/ListarPacientes", DisplayOnScreen = true, Status = Status.Active, Menu = menuCadastros });
			menuCadastros.Features.Add(new Feature { Description = "Cadastro de Paciente", Sequence = 2, Url = "~/Pacientes/Novo", DisplayOnScreen = false, Status = Status.Active, Menu = menuCadastros });
			menuCadastros.Features.Add(new Feature { Description = "Editar Paciente", Sequence = 3, Url = "~/Pacientes/EditarPaciente", DisplayOnScreen = false, Status = Status.Active, Menu = menuCadastros });

			menuList.Add(menuCadastros);

			var menuAtendimentos = new Menu { Description = "Atendimentos", Sequence = 3, Status = Status.Active };

			menuList.Add(menuAtendimentos);

			return menuList;
		}
	}
}