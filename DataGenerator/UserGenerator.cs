﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using NeuroCloud.Domain.Entities;
using NeuroCloud.Domain.Enums;
using NeuroCloud.Domain.Repositories;

namespace DataGenerator
{
	public class UserGenerator
	{
		private IUserRepository UserRepository { get; set; }

		public UserGenerator(IUserRepository userRepository)
		{
			this.UserRepository = userRepository;
		}

		public IList<User> GenerateUsers(IList<Sector> sectors, IList<Group> groups)
		{
			var users = CreateObjects(sectors, groups);

			foreach (var user in users)
			{
				this.UserRepository.Save(user);
			}

			return users;
		}

		private IList<User> CreateObjects(IList<Sector> sectors, IList<Group> groups)
		{
			var usersList = new List<User>();

			var userAdmin = new User
				                {
					                FirstName = "Plácido Monteiro",
					                LastName = "Dinelli Bisneto",
					                CPF = "530.654.762-15",
					                CreatedAt = DateTime.Now,
					                RG = "17329981",
					                Email = "cidico@live.com",
					                Password = "123",
					                Record = "12345",
					                BirthDate = new DateTime(1986, 10, 11),
					                Status = Status.Active,
					                Username = "cidico",
					                Sector = sectors[new Random().Next(0, sectors.Count - 1)]
				                };

			foreach (var @group in groups)
			{
				userAdmin.AddToGroup(group);
			}

			usersList.Add(userAdmin);

			Thread.Sleep(100);

			var userFulano = new User
				                 {
					                 FirstName = "Fulano de Tal",
					                 LastName = "da Silva e Silva",
					                 CPF = "11111111111",
					                 CreatedAt = DateTime.Now,
					                 RG = "11111111",
					                 Email = "lalala@live.com",
					                 Username = "lalala",
					                 Password = "123",
					                 Record = "12345",
					                 BirthDate = new DateTime(1988, 11, 01),
					                 Status = Status.Active,
					                 Sector = sectors[new Random().Next(0, sectors.Count - 1)]
				                 };

			userFulano.AddToGroup(groups[1]);

			usersList.Add(userFulano);

			Thread.Sleep(100);

			var userHep = new User
				              {
					              FirstName = "Herp Derp",
					              LastName = "Lero Lero",
					              CPF = "22222222222",
					              CreatedAt = DateTime.Now,
					              RG = "222222",
					              Email = "herpderp@live.com",
					              Username = "herpderp",
					              Password = "123",
					              Record = "12345",
					              BirthDate = new DateTime(1988, 06, 12),
					              Status = Status.Active,
					              Sector = sectors[new Random().Next(0, sectors.Count - 1)]
				              };

			userHep.AddToGroup(groups[2]);
			usersList.Add(userHep);

			Thread.Sleep(100);

			return usersList;
		}
	}
}