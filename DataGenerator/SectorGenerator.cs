﻿using System.Collections.Generic;
using NeuroCloud.Domain.Entities;
using NeuroCloud.Domain.Repositories;

namespace DataGenerator
{
	public class SectorGenerator
	{
		private ISectorRepository SectorRepository { get; set; }

		public SectorGenerator(ISectorRepository sectorRepository )
		{
			this.SectorRepository = sectorRepository;
		}

		public IList<Sector> GenerateSectors()
		{
			var sectors = this.CreateObjects();

			foreach (var sector in sectors)
			{
				this.SectorRepository.Save(sector);
			}

			return sectors;
		}

		private IList<Sector> CreateObjects()
		{
			var sectorsList = new List<Sector>();

			sectorsList.Add(new Sector { Description = "Enfermaria"});
			sectorsList.Add(new Sector { Description = "Pediatria" });
			sectorsList.Add(new Sector { Description = "Neurologia" });
			sectorsList.Add(new Sector { Description = "U.T.I" });
			sectorsList.Add(new Sector { Description = "Cardiologia" });

			return sectorsList;
		}
	}
}