﻿using System.Collections.Generic;
using NeuroCloud.Domain.Entities;
using NeuroCloud.Domain.Repositories;

namespace DataGenerator
{
	public class FeaturesGenerator
	{
		private IFeatureRepository FeatureRepository { get; set; }

		public FeaturesGenerator(IFeatureRepository featureRepository)
		{
			this.FeatureRepository = featureRepository;
		}

		public IList<Feature> GenerateFeatures()
		{
			var features = CreateObjects();

			foreach (var feature in features)
			{
				this.FeatureRepository.Save(feature);
			}

			return features;
		}

		private static IList<Feature> CreateObjects()
		{
			var featuresList = new List<Feature>();

			// Features para Usuários
			featuresList.Add(new Feature { Description = "Listar Usuários", Sequence = 1, Url = "~/Usuarios/ListarUsuarios" });
			featuresList.Add(new Feature { Description = "Cadastro de Usuário", Sequence = 2, Url = "~/Usuarios/Novo" });
			featuresList.Add(new Feature { Description = "Editar de Usuário", Sequence = 3, Url = "~/Usuarios/EditarUsuario" });

			// Features para Grupos
			featuresList.Add(new Feature { Description = "Listar Grupos", Sequence = 1, Url = "~/Grupos/ListarGrupos" });
			featuresList.Add(new Feature { Description = "Cadastro de Grupo", Sequence = 2, Url = "~/Grupos/Novo" });
			featuresList.Add(new Feature { Description = "Editar de Grupo", Sequence = 3, Url = "~/Grupos/EditarGrupo" });

			// Features para Leitos
			featuresList.Add(new Feature { Description = "Listar Leitos", Sequence = 1, Url = "~/Leitos/ListarLeitos" });
			featuresList.Add(new Feature { Description = "Cadastro de Leito", Sequence = 2, Url = "~/Leitos/Novo" });
			featuresList.Add(new Feature { Description = "Editar de Leito", Sequence = 3, Url = "~/Leitos/EditarLeito" });

			// Features para Quartos
			featuresList.Add(new Feature { Description = "Listar Quartos", Sequence = 1, Url = "~/Quartos/ListarQuartos" });
			featuresList.Add(new Feature { Description = "Cadastro de Quarto", Sequence = 2, Url = "~/Quartos/Novo" });
			featuresList.Add(new Feature { Description = "Editar de Quarto", Sequence = 3, Url = "~/Quartos/EditarQuarto" });

			return featuresList;
		}
	}
}