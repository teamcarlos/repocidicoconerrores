﻿using System.Collections.Generic;
using NeuroCloud.Domain.Entities;
using NeuroCloud.Domain.Enums;
using NeuroCloud.Domain.Repositories;

namespace DataGenerator
{
	public class GroupGenerator
	{
		private IGroupRepository GroupRepository { get; set; }

		public GroupGenerator(IGroupRepository groupRepository)
		{
			GroupRepository = groupRepository;
		}

		public IList<Group> GenerateGroups(IList<Menu> menus)
		{
			var groups = this.CreateObjects(menus);

			foreach (var @group in groups)
			{
				this.GroupRepository.Save(group);
			}

			return groups;
		}

		private IList<Group> CreateObjects(IList<Menu> menus)
		{
			var groupList = new List<Group>();

			var adminsGroup = new Group {Description = "Administradores", Status = Status.Active};

			foreach (var menu in menus)
				adminsGroup.Menus.Add(new GroupMenu { Group = adminsGroup, Menu = menu, Status = Status.Active });

			groupList.Add(adminsGroup);

			var medicGroups = new Group {Description = "Médicos", Status = Status.Active};
			
			foreach (var menu in menus)
				medicGroups.Menus.Add(new GroupMenu { Group = medicGroups, Menu = menu, Status = Status.Active });

			groupList.Add(medicGroups);


			var nursesGroup = new Group { Description = "Enfermeiros", Status = Status.Active };

			foreach (var menu in menus)
				nursesGroup.Menus.Add(new GroupMenu { Group = nursesGroup, Menu = menu, Status = Status.Active });

			groupList.Add(nursesGroup);

			var neuroGroups = new Group { Description = "Neurologistas", Status = Status.Active };

			foreach (var menu in menus)
				neuroGroups.Menus.Add(new GroupMenu { Group = neuroGroups, Menu = menu, Status = Status.Active });

			groupList.Add(neuroGroups);

			return groupList;
		}
	}
}