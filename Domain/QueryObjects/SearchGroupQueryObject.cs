﻿namespace NeuroCloud.Domain.QueryObjects
{
    public class SearchGroupQueryObject : QueryObjectBase
    {
        public string Description { get; set; }
    }
}
