﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NeuroCloud.Domain.QueryObjects
{
	public class SearchRoomQueryObject : QueryObjectBase
	{
		public string Description { get; set; }

		public long SectorId { get; set; }
	}
}
