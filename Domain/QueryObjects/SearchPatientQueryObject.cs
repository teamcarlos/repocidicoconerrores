﻿namespace NeuroCloud.Domain.QueryObjects
{
    public class SearchPatientQueryObject : QueryObjectBase
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string CPF { get; set; }
    }
}