﻿using System;

namespace NeuroCloud.Domain.QueryObjects
{
    public class QueryObjectBase
    {
        public QueryObjectBase()
        {
            this.RecordPerPage = 12;
            this.CurrentPage = 1;
        }

        public Int64 Id { get; set; }

        public int CurrentPage { get; set; }

        public int RecordPerPage { get; set; }

        public long RecordCount { get; set; }
    }
}