﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NeuroCloud.Domain.QueryObjects
{
    public class SearchUserQueryObject : QueryObjectBase
    {
        public string Name { get; set; }
        public Int64 SectorId { get; set; }
    }
}
