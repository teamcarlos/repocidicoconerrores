﻿namespace NeuroCloud.Domain.Components
{
	public class Address
	{
		public virtual string Street { get; set; }
		public virtual  string	Number { get; set; }
		public virtual string Complement { get; set; }
		public virtual string District { get; set; }
		public virtual string City { get; set; }
		public virtual string State { get; set; }
		public virtual string ZipCode { get; set; }
	}
}