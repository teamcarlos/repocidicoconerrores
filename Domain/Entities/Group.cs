using System;
using System.Collections.Generic;
using System.Linq;
using Iesi.Collections.Generic;
using NeuroCloud.Domain.Enums;

namespace NeuroCloud.Domain.Entities
{
	public class Group : EntityBase
	{
		public Group()
		{
            this.Menus = new HashedSet<GroupMenu>();
		}

		public virtual string Description { get; set; }

	    public virtual Status Status { get; set; }

	    public virtual ICollection<GroupMenu> Menus { get; set; }
	}
}