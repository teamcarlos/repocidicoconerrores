﻿using System.Collections.Generic;
using Iesi.Collections.Generic;
using NeuroCloud.Domain.Enums;

namespace NeuroCloud.Domain.Entities
{
	public class Room : EntityBase
	{
		public Room()
		{
			this.Beds = new HashedSet<Bed>();
		}

		public virtual string Description { get; set; }

		public virtual Status Status { get; set; }

		public virtual Sector Sector { get; set; }

		public virtual RoomType Type { get; set; }

		public virtual ICollection<Bed> Beds { get; set; }
	}
}