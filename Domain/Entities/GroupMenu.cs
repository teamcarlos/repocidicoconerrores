using NeuroCloud.Domain.Enums;

namespace NeuroCloud.Domain.Entities
{
    public class GroupMenu : EntityBase
    {
        public virtual Group Group { get; set; }

        public virtual Menu Menu { get; set; }

        public virtual Status Status { get; set; }
    }
}