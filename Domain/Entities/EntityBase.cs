using System;

namespace NeuroCloud.Domain.Entities
{
	public class EntityBase
	{
		public virtual Int64 Id { get; set; }
	}
}