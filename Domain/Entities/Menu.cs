using System;
using System.Collections.Generic;
using Iesi.Collections.Generic;
using NeuroCloud.Domain.Enums;

namespace NeuroCloud.Domain.Entities
{
	public class Menu : EntityBase
	{
		public Menu()
		{
			this.Features = new HashedSet<Feature>();
		    this.Groups = new HashedSet<GroupMenu>();
		}

		public virtual string Description { get; set; }
		
		public virtual int Sequence { get; set; }
		
		public virtual Status Status { get; set; }
		
		public virtual DateTime? BlockedAt { get; set; }

		public virtual ICollection<Feature> Features { get; set; }

	    public virtual ICollection<GroupMenu> Groups { get; set; }
	}
}