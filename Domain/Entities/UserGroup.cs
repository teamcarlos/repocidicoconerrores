using NeuroCloud.Domain.Enums;

namespace NeuroCloud.Domain.Entities
{
	public class UserGroup : EntityBase
	{
		public virtual User User { get; set; }
		public virtual Group Group { get; set; }

		public virtual Status Status { get; set; }
	}
}