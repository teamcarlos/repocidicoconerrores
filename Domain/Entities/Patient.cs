﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NeuroCloud.Domain.Components;
using NeuroCloud.Domain.Enums;

namespace NeuroCloud.Domain.Entities
{
	public class Patient : EntityBase
	{
		public virtual string FirstName { get; set; }
		public virtual string LastName { get; set; }
		public virtual string CPF { get; set; }
		public virtual string RG { get; set; }
		public virtual DateTime BirthDate { get; set; }
		public virtual string Email { get; set; }
		public virtual DateTime CreatedAt { get; set; }
		public virtual Sex Sex { get; set; }
	    public virtual Status Status { get; set; }
        public virtual Address Address { get; set; }
		public virtual string HomePhone { get; set; }
		public virtual string CellPhone { get; set; }

	}
}
