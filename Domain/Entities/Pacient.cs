﻿using System;

namespace NeuroCloud.Domain.Entities
{
	public class Pacient : EntityBase
	{
		public virtual string FirstName { get; set; }
		
		public virtual string LastName { get; set; }
		
		public virtual string CPF { get; set; }
		
		public virtual string RG { get; set; }
		
		public virtual DateTime? BirthDate { get; set; }
		
		public virtual Pacient PersonInCharge { get; set; }
	}
}