using System;
using System.Collections.Generic;
using Iesi.Collections.Generic;
using NeuroCloud.Domain.Enums;

namespace NeuroCloud.Domain.Entities
{
	public class User : EntityBase
	{
		public User()
		{
			this.Groups = new HashedSet<UserGroup>();
		}

		public virtual string FirstName { get; set; }
		public virtual string LastName { get; set; }
		public virtual string CPF { get; set; }
		public virtual string RG { get; set; }
		public virtual DateTime BirthDate { get; set; }
		public virtual string Email { get; set; }
		public virtual string Record { get; set; }
		public virtual DateTime CreatedAt { get; set; }
		public virtual string Username { get; set; }
		public virtual string Password { get; set; }
		public virtual Status Status { get; set; }
		public virtual DateTime? BlockedAt { get; set; }
		public virtual Sector Sector { get; set; }

		public virtual ICollection<UserGroup> Groups { get; set; }

		public virtual void AddToGroup(Group group)
		{
			if (group == null)
				throw new ArgumentNullException("Group");

			this.Groups.Add(new UserGroup
				                {
					                Group = group,
					                User = this,
					                Status = Status.Active
				                });
		}
	}
}