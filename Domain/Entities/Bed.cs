﻿using NeuroCloud.Domain.Enums;

namespace NeuroCloud.Domain.Entities
{
	public class Bed : EntityBase
	{
		public virtual int Number { get; set; }

		public virtual string Description { get; set; }

		public virtual Situation Situation { get; set; }

		public virtual Room Room { get; set; }
	}
}
