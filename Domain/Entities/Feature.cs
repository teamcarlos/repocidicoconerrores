using NeuroCloud.Domain.Enums;

namespace NeuroCloud.Domain.Entities
{
	public class Feature : EntityBase
	{
		public virtual string Description { get; set; }
		
		public virtual int Sequence { get; set; }
		
		public virtual string Url { get; set; }
		
		public virtual Status Status { get; set; }

		public virtual bool DisplayOnScreen { get; set; }
		
		public virtual Menu Menu { get; set; }
	}
}