﻿namespace NeuroCloud.Domain.Enums
{
	public enum Situation
	{
		Free = 1,
		Occupied = 2,
		Cleaning = 4
	}
}
