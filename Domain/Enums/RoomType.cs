﻿namespace NeuroCloud.Domain.Enums
{
	public enum RoomType
	{
		Private = 1,
		Collective = 2
	}
}