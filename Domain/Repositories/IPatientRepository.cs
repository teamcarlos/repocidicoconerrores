using System.Collections.Generic;
using NeuroCloud.Domain.Entities;
using NeuroCloud.Domain.QueryObjects;

namespace NeuroCloud.Domain.Repositories
{
	public interface IPatientRepository : IRepositoryBase<Patient>
	{
	    ICollection<Patient> Search(SearchPatientQueryObject queryObject);
	}
}