﻿using System.Collections.Generic;
using NeuroCloud.Domain.Entities;

namespace NeuroCloud.Domain.Repositories
{
	public interface IMenuRepository : IRepositoryBase<Menu>
	{
		IList<Menu> GetAll();
	}
}