using System.Collections.Generic;
using NeuroCloud.Domain.Entities;
using NeuroCloud.Domain.QueryObjects;

namespace NeuroCloud.Domain.Repositories
{
	public interface IUserRepository : IRepositoryBase<User>
	{
	    bool EmailExists(string email);
		
		bool LoginExists(string login);
	    
		ICollection<User> SearchUsers(SearchUserQueryObject queryObject);
		
		User Login(string username, string password);
	}
}