using System.Collections.Generic;
using NeuroCloud.Domain.Entities;
using NeuroCloud.Domain.QueryObjects;

namespace NeuroCloud.Domain.Repositories
{
    public interface IGroupRepository : IRepositoryBase<Group>
    {
        ICollection<Group> GetAll();
        ICollection<Group> SearchGroups(SearchUserQueryObject queryObject);
    }
}