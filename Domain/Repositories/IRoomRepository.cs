﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NeuroCloud.Domain.Entities;
using NeuroCloud.Domain.QueryObjects;

namespace NeuroCloud.Domain.Repositories
{
    public interface IRoomRepository : IRepositoryBase<Room>
    {
	    IList<Room> SearchRooms(SearchRoomQueryObject queryObject);
    }
}
