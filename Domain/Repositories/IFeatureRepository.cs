﻿using NeuroCloud.Domain.Entities;

namespace NeuroCloud.Domain.Repositories
{
	public interface IFeatureRepository : IRepositoryBase<Feature>
	{
	}
}
