using System;
using NeuroCloud.Domain.Entities;

namespace NeuroCloud.Domain.Repositories
{
	public interface IRepositoryBase<T> where T : EntityBase
	{
		T Save(T obj);

		void Delete(T obj);

		T GetById(Int64 id);

		T[] GetByIds(Int64[] ids);
	}
}