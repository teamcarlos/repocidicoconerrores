﻿using System.Collections.Generic;
using NeuroCloud.Domain.Entities;

namespace NeuroCloud.Domain.Repositories
{
	public interface ISectorRepository : IRepositoryBase<Sector>
	{
		IList<Sector> GetAllSectors();
	}
}
